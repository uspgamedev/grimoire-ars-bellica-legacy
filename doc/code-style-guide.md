
# Code Style Guide

This style guide complements the formatting done by clang-format.

## Naming protocol

1. Type names are `CamelCase`;
2. Method/function names `snake_case`;
3. Private attributes are `_`-prefixed `snake_case`;
4. Public  attributes are `snake_case`;
5. Namespaces are `lowercase`;

