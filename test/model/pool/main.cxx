
#include "model/pool.h"

#include <exception>
#include <iostream>
#include <unordered_set>

namespace game::model::test {

namespace {

using std::logic_error;
using std::string;
using std::to_string;
using std::unordered_set;

void _assert_throw(bool check, const string &msg = "logic error") {
  if (!check) throw logic_error(msg);
}

#define ASSERT_THROW(check, msg) \
  _assert_throw(check, string(__FILE__) + ":" + to_string(__LINE__) + ":\n\t" #check " // " + msg)

struct Object {
  ID id{ 0 };
  ID get_id() const { return id; }
};

void test_single_object() {
  Pool<Object> pool;
  auto obj = pool.emplace_new(Object{ 42 });
  ASSERT_THROW(obj.is_valid(), "newly pooled object should be valid");
  ASSERT_THROW(pool.at(42).is_valid(), "searched object should be valid");
  pool.erase(obj);
  ASSERT_THROW(!obj.is_valid(), "erased object should be invalid");
  ASSERT_THROW(!pool.at(42).is_valid(), "searched object should be invalid");
}

void test_single_object_const() {
  Pool<Object> pool;
  pool.emplace_new(Object{ 42 });
  ASSERT_THROW(pool.at(42).to_const().is_valid(), "searched object as const should be valid");
  const Pool<Object> &cpool = pool;
  auto obj = cpool.at(42);
  ASSERT_THROW(obj.is_valid(), "newly pooled object should be valid");
  ASSERT_THROW(cpool.at(42).is_valid(), "searched object should be valid");
  pool.erase(pool.at(42));
  ASSERT_THROW(!obj.is_valid(), "erased object should be invalid");
  ASSERT_THROW(!cpool.at(42).is_valid(), "searched object should be invalid");
}

void test_stale_object() {
  Pool<Object> pool;
  auto obj = pool.emplace_new(Object{ 42 });
  ASSERT_THROW(obj.is_valid(), "newly pooled object should be valid");
  ASSERT_THROW(pool.at(42).is_valid(), "searched object should be valid");
  pool.erase(obj);
  pool.emplace_new(Object{ 1337 });
  ASSERT_THROW(!obj.is_valid(), "stale object should be invalid");
  ASSERT_THROW(!pool.at(42).is_valid(), "searched object should be invalid");
}

void test_iterate() {
  Pool<Object> pool;
  pool.emplace_new(Object{ 42 });
  pool.emplace_new(Object{ 1337 });
  unordered_set<ID> seen;
  for (auto obj : pool) seen.insert(obj->get_id());
  ASSERT_THROW(seen.find(42) != seen.end(), "should have object 42");
  ASSERT_THROW(seen.find(1337) != seen.end(), "should have object 1337");
}

void test_iterate_const() {
  Pool<Object> pool;
  pool.emplace_new(Object{ 42 });
  pool.emplace_new(Object{ 1337 });
  unordered_set<ID> seen;
  const Pool<Object> &cpool = pool;
  for (auto obj : cpool) seen.insert(obj->get_id());
  ASSERT_THROW(seen.find(42) != seen.end(), "should have object 42");
  ASSERT_THROW(seen.find(1337) != seen.end(), "should have object 1337");
}

void test_invalid_handle() {
  Handle<Object> invalid{};
  bool exception_thrown = false;
  try {
    invalid->get_id();
  } catch (std::exception &e) {
    exception_thrown = true;
  }
  ASSERT_THROW(exception_thrown, "should have thrown error");
}

#undef ASSERT_THROW

} // namespace

} // namespace game::model::test

int main() try {
  using namespace game::model::test;
  test_single_object();
  test_single_object_const();
  test_stale_object();
  test_iterate();
  test_iterate_const();
  test_invalid_handle();
  return 0;
} catch (std::exception &e) {
  std::cerr << e.what() << std::endl;
  return -1;
}
