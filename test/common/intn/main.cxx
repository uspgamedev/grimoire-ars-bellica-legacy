
#include "common/intn.h"

#include <cassert>
#include <iostream>

namespace {

using game::Int2;
using game::Int3;
using game::to_int2;
using game::to_int3;

using std::cout;
using std::endl;
using std::is_constructible;

} // namespace

int main() {

  const static size_t I{ 5 }, J{ 6 }, K{ 7 };
  const static int X{ 1 }, Y{ 2 }, Z{ 3 };

  // Check default constructors
  Int2 vec2(X, Y);
  Int3 vec3(X, Y, Z);

  // Check casting
  const Int2 u2 = to_int2(J, I);
  const Int3 u3 = to_int3(J, I, K);

  // Sum
  const Int2 sum_result2 = vec2 + u2;
  const Int3 sum_result3 = vec3 + u3;

  // Mul
  const Int2 mul_result2 = vec2 * u2;
  const Int3 mul_result3 = vec3 * u3;

  // Dot
  const int dot = vec3.dot(Int3(2, 2, 2));

  try {
    // Assert sum results
    assert(sum_result2 == to_int2(X + J, Y + I));
    assert(sum_result3 == to_int3(X + J, Y + I, Z + K));
    // Assert mul results
    assert(mul_result2 == to_int2(X * J, Y * I));
    assert(mul_result3 == to_int3(X * J, Y * I, Z * K));
    // Assert dot results
    assert(dot == 12);
    // Assert unit methods
    assert(Int3::unit_x() == Int3(1, 0, 0));
    assert(Int3::unit_y() == Int3(0, 1, 0));
    assert(Int3::unit_z() == Int3(0, 0, 1));
    // Assert component getters
    assert(vec3.get_x_component() == Int3(X, 0, 0));
    assert(vec3.get_y_component() == Int3(0, Y, 0));
    assert(vec3.get_z_component() == Int3(0, 0, Z));
    // Assert component setters
    vec2.set_y(42);
    vec3.set_j(781);
    vec3.set_z(42);
    assert(vec2.get_y() == 42);
    assert(vec3.get_j() == 781);
    assert(vec3.get_z() == 42);
  } catch (...) {
    cout << "INTN TEST FAILED!" << endl;
    return -1;
  }

  return 0;
}
