
## Code Standards

* Capitalization:
  * Classes, Structs, and such should be `PascalCase`:
    * `class CoolClass; struct Hella;`
  * Class members (both methods and attributes) should be `snake_case`.
  * Local variables and arguments can be either `lowercase` or `snake_case`.
  * Macros are `UPPERCASE`, unless they substitute a type, in which case they are `PascalCase`. Prefer C++'s `using` or `constexpr` rather than C's `#define`:
    * `#define COOL_GLOBAL 42             // don't do this!`
    * `constexpr size_t HARD_LIMIT = 100; // that's cool, though`
    * `using ElementID = uint64_t;        // also ok`
* Class declarations should not contain method implementations within.
  * Write an inline method in the same header instead.
  * Or consider whether it should not be inline at all. If the method is not template and depends on other files, it definitely should not be inline.
* Types in public interfaces should prefer references to pointers, except for polymorphic types.

## Workflow Standards

* Run make beautiful after finishing your branch.
  * Be sure to run it with clang-format >= 6. Previous versions of clang-format will format things differently.
  * Don't run clang-format manually unless you know what you're doing! Use the generated makefile 'beautiful' target instead.
* Run make check and make tidy targets locally after finishing your branch, if possible.
  * The CI will do it for you if you can't/don't want to, but you can save us some nasty email notifications if you check it locally first.
  * You can run them with multiple jobs to go faster. Tidy is especially slow.
* Sort header includes with the following priority:
  1. Project headers
  2. Library headers (e.g. "ncurses.h")
  3. C++ STL headers
  4. C standard library headers
  * Separate each of these include groups with an empty line
  * An exception to this rule is `portable/filesystem.h`, which should be considered a C++ STL header
* Regarding Merge Requests (MRs):
  * Be sure to check the 'delete branch after merging' checkbox.
  * All MRs have to be approved by at least 2 other maintainers.

## Adding Features

* Git Gud
* When Creating an `Action`:
  * Do not presume anything about the object you're interacting with.
  * Do not presume anything about the invoker other than it is an actor.
