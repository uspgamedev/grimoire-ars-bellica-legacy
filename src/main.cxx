
#include "common/appdata.h"
#include "common/intn.h"
#include "common/log.h"
#include "common/settings.h"
#include "config.h"
#include "frontend/ui.h"
#include "model/load_session.h"
#include "model/session.h"
#include "portable/filesystem.h"
#include "state/dead.h"
#include "state/entityselection.h"
#include "state/lookmode.h"
#include "state/play.h"
#include "state/stack.h"
#include "state/statearguments.h"
#include "state/userturn.h"
#include "subsystemrack.h"

#include <exception>
#include <iostream>
#include <vector>

#include <csignal>

namespace {

using game::Log;
using game::Settings;
using game::SubsystemRack;
using game::model::create_session_file;
using game::model::load_session;
using game::model::save_session;
using game::state::Argument;
using game::state::Dead;
using game::state::EntitySelection;
using game::state::LookMode;
using game::state::Message;
using game::state::Play;
using game::state::Stack;
using game::state::UserTurn;

using std::cerr;
using std::endl;
using std::string;

// for signal interrupts
SubsystemRack *rack;

void interrupt_handler(int /*signal*/) {
  rack->ui.emergency_exit();
  exit(0);
}

} // unnamed namespace

/**
 *  ./grimoire [--new] <name>
 */

int main(int argc, const char **argv) {
  if (!game::appdata::bootstrap()) {
    cerr << "Failed to initialize database" << endl;
    return -1;
  }

  string save_name{};
  bool new_save{ false }; // this is needed because I DON'T KNOW
  bool del_save{ false };
  while (--argc > 0) {
    string param = *++argv;
    if (param == "--new")
      new_save = true;
    else if (param == "--over") {
      new_save = true;
      del_save = true;
    } else if (param == "--del")
      del_save = true;
    else if (save_name.empty())
      save_name = param;
  }

  if (save_name.empty()) {
    cerr << "Please provide a save name" << endl;
    return -1;
  }

  // system logger
  const static Log LOG("system");

  try {
    Settings settings;
    if (del_save) {
      std::filesystem::remove(game::appdata::save_dir() / (save_name + ".toml"));
      if (!new_save) return 0;
    }
    if (new_save) {
      LOG("Creating new save: `%s`", save_name);
      create_session_file(save_name);
    }

    LOG("Initializing sesssion...");
    SubsystemRack subsystemrack{ game::model::Session{ save_name }, game::frontend::UI{} };
    load_session(subsystemrack.session);
    rack = &subsystemrack;

    Stack stack(subsystemrack);

    LOG("Registering Interrupt Signals...");
    // ctrl + c
    std::signal(SIGINT, interrupt_handler);
    // ctrl + slash
    std::signal(SIGQUIT, interrupt_handler);

    LOG("Registering gamestates...");
    stack.register_state<Play>("PLAY");
    stack.register_state<UserTurn>("USER_TURN");
    stack.register_state<LookMode>("LOOK_MODE");
    stack.register_state<EntitySelection>("SELECT_MODE");
    stack.register_state<Dead>("DEAD");

    LOG("Setting initial gamestate...");
    stack.push("PLAY");

    LOG("Initialization complete");
    LOG("Game version %", GRIMOIRE_VERSION);
    LOG("Profile version %", settings.get_version());

    // Welcome message to player
    subsystemrack.session.message(L"Welcome!");
    while (stack.has_state()) {
      while (stack.update()) subsystemrack.ui.flush();
      if (stack.has_state()) subsystemrack.ui.present(subsystemrack.session);
    }

    save_session(subsystemrack.session);
  } catch (std::filesystem::filesystem_error &e) {
    cerr << e.path1() << endl;
  } catch (std::runtime_error &e) {
    cerr << e.what() << endl;
  } /* catch (...) {
    cerr << "Something went wrong. Aborting." << endl;
  } */

  return 0;
}
