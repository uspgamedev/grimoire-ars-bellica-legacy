
#ifndef GRIMOIRE_COMMON_SUBSYSTEMRACK_H_
#define GRIMOIRE_COMMON_SUBSYSTEMRACK_H_

#include "frontend/ui.h"
#include "model/session.h"

namespace game {

struct SubsystemRack final {
  model::Session session;
  frontend::UI ui;
};

} // namespace game

#endif // GRIMOIRE_COMMON_SUBSYSTEMRACK_H_
