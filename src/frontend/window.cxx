
#include "frontend/window.h"

#include "common/log.h"
#include "frontend/colors.h"

namespace game::frontend {

namespace {
using std::string;
using std::wstring;
} // unnamed namespace

Window::Window() { reset(_size, _pos); }

void Window::reset(const Int2 &pos, const Int2 &size) {
  if (_win != nullptr) {
    ::delwin(_win);
  }

  _pos = pos;
  _size = size;
  _win = ::newwin(size[1], size[0], pos[1], pos[0]);
}

void Window::clear() { ::werase(_win); }

void Window::highlight(const Int2 &pos) {
  // NOLINTNEXTLINE(hicpp-signed-bitwise)
  mvwchgat(_win, pos.get_y(), pos.get_x(), 1, A_REVERSE, TO_NATIVE(ColorPairID::DEFAULT), nullptr);
}

void Window::write(const Int2 &pos, const string &text) {
  mvwaddstr(_win, pos.get_y(), pos.get_x(), text.c_str());
}

void Window::write(const Int2 &pos, const wstring &text) {
  mvwaddwstr(_win, pos.get_y(), pos.get_x(), text.c_str());
}

void Window::write(const Int2 &pos, const wchar_t text) {
  wstring tmp(1, text);
  write(pos, tmp);
}

} // namespace game::frontend
