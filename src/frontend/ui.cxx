
#include "frontend/ui.h"

#include "common/directions.h"
#include "common/log.h"
#include "common/settings.h"
#include "frontend/colors.h"
#include "frontend/command.h"
#include "frontend/layout.h"
#include "frontend/log_window.h"
#include "frontend/map_window.h"
#include "frontend/status_window.h"
#include "model/components/damageable.h"
#include "model/components/location.h"
#include "model/components/map.h"
#include "model/components/player.h"
#include "model/components/visible.h"
#include "model/glyph_palette.h"
#include "model/tile_map.h"
#include "model/world.h"

#include "frontend/curses.h"

#include <array>
#include <clocale>
#include <unordered_map>

namespace game::frontend {

namespace {
using game::DIRECTIONS;
using game::Settings;
using namespace game::frontend::layout;
using game::frontend::Command;
using game::model::Handle;
using game::model::Session;
using game::model::components::Location;
using game::model::components::Map;
using TileID = game::model::TileMap::TileID;
using std::nullopt;
using std::optional;

} // unnamed namespace

struct UI::Impl {

  Impl();
  void adjust_layout();
  Command poll_command();
  Int2 get_movement() const;
  void clean_up();

  LogWindow logwin;
  MapWindow mapwin;
  StatusWindow statuswin;
  int scrwidth{ 0 }, scrheight{ 0 };
  Command current_cmd{ Command::INVALID };
  bool input_enabled{ false };
  Settings settings{};
  game::Log log;
};

UI::UI() : _valid(true), _impl(new Impl) {}

UI::~UI() {
  delete _impl;
  if (_valid) endwin();
}

void UI::flush() { _impl->current_cmd = Command::INVALID; }

void UI::enable_input() { _impl->input_enabled = true; }

void UI::disable_input() { _impl->input_enabled = false; }

void UI::present(const Session &session) {
  _impl->adjust_layout();
  _impl->logwin.show(session);
  _impl->statuswin.show(session);
  _impl->mapwin.show(session);
  if (_impl->input_enabled) _impl->current_cmd = _impl->poll_command();
}

optional<Int3> UI::entity_navigation() {
  _impl->mapwin.lock_cursor();
  Int2 movement = _impl->get_movement();
  if (movement.size() > 0) {
    return static_cast<Int3>(movement);
  }
  return nullopt;
}

bool UI::cursor_navigation(Handle<const Map> map, Handle<const Location> player) {
  _impl->mapwin.unlock_cursor();
  Int2 movement = _impl->get_movement();
  if (movement.size() > 0) {
    Int2 new_pos = _impl->mapwin.get_cursor_position() + movement;
    Int3 map_pos = static_cast<Int3>(new_pos) + player->get_position().get_k_component();
    if (map->is_tile_inside(map_pos)) {
      _impl->mapwin.set_cursor_position(new_pos);
      return true;
    }
  }
  return false;
}

Int2 UI::cursor_position() const { return _impl->mapwin.get_cursor_position(); }

bool UI::inspect() { return _impl->current_cmd == Command::INSPECT; }

bool UI::interact() { return _impl->current_cmd == Command::INTERACT; }

bool UI::pickup() {
  _impl->log("cmd %", static_cast<int>(_impl->current_cmd));
  return _impl->current_cmd == Command::PICKUP;
}

bool UI::quit() { return _impl->current_cmd == Command::QUIT; }

void UI::emergency_exit() {
  _impl->clean_up();
  _valid = false;
}

UI::Impl::Impl() : logwin(), mapwin(), statuswin(), log("ui") {
  setlocale(LC_CTYPE, "");
  initscr();
  raw();
  keypad(stdscr, true);
  // halfdelay(1); // maybe? real-time stuff
  noecho();
  cbreak();
  curs_set(0);
  start_color();
  // Yes, it starts from 1. See man curs_color.
  for (size_t i = 1; i < COLOR_PAIR_DEFS().size(); i++) {
    auto [fg, bg] = COLOR_PAIR_DEFS().at(i);
    init_pair(static_cast<short>(i), fg, bg);
  }
  log("Initializing UI...");
  log("Max color pairs: %", COLOR_PAIRS);
  refresh();
}

Command UI::Impl::poll_command() {
  const auto &keybindings = settings.get_keybindings();
  auto ch = static_cast<char>(getch());
  if (keybindings.find(ch) == keybindings.end()) return Command::INVALID;
  return keybindings.at(ch);
}

Int2 UI::Impl::get_movement() const {
  auto idx = static_cast<size_t>(current_cmd);
  if (idx < DIRECTIONS.size()) return DIRECTIONS.at(idx);
  return Int2(0, 0);
}

void UI::Impl::adjust_layout() {
  int newheight, newwidth;
  getmaxyx(stdscr, newheight, newwidth);
  if (newheight != scrheight || newwidth != scrwidth) {
    // update screen dimension
    scrheight = newheight;
    scrwidth = newwidth;
    clear();
    refresh();
    Int2 screen_size(scrwidth, scrheight);
    logwin.adjust(screen_size);
    mapwin.adjust(screen_size);
    statuswin.adjust(screen_size);
  }
}

void UI::Impl::clean_up() {
  clear();
  refresh();
  endwin();
}

} // namespace game::frontend
