
#include "frontend/map_window.h"
#include "frontend/window.h"

#include "frontend/layout.h"
#include "model/appearance.h"
#include "model/components/location.h"
#include "model/components/map.h"
#include "model/components/player.h"
#include "model/components/visible.h"
#include "model/glyph_palette.h"
#include "model/session.h"
#include "model/tile_map.h"

namespace game::frontend {

namespace {
using game::model::Appearance;
using game::model::GlyphPalette;
using game::model::Session;
using game::model::components::Location;
using game::model::components::Map;
using game::model::components::Visible;
using TileID = game::model::TileMap::TileID;
using namespace layout;

wchar_t _TILE_GLYPH(TileID tile_id) {
  const static GlyphPalette palette;
  return palette.tile_to_glyph(tile_id);
}

wchar_t _ENTITY_GLYPH(Appearance appearance) {
  const static GlyphPalette palette;
  return palette.appearance_to_glyph(appearance);
}
} // namespace

MapWindow::MapWindow() : _win(new Window()) {}

// required by the incomplete type unique ptr
MapWindow::~MapWindow() = default;

void MapWindow::adjust(Int2 screen_size) {
  int base_x = (screen_size.get_x() - STATUS_WIDTH) / 2 - (2 * MAP_WIDTH / 2 - 1);
  int base_y = (screen_size.get_y() - LOG_HEIGHT) / 2 - MAP_HEIGHT / 2;
  _win->reset(Int2(base_x, base_y), Int2(2 * MAP_WIDTH - 1, MAP_HEIGHT));
}

void MapWindow::show(const Session &session) {
  auto map = session.world.get_map();
  Int3 camera_pos = _last_camera_pos;
  if (auto player = session.world.get_player())
    camera_pos = player->select<Location>()->get_position();
  const auto center = Int2(MAP_WIDTH / 2, MAP_HEIGHT / 2);
  const auto scale = Int2(2, 1);
  _win->clear();
  if (_cursor_locked) _cursor_position = static_cast<Int2>(camera_pos);
  for (int y = -MAP_HEIGHT / 2; y <= MAP_HEIGHT / 2; y++) {
    for (int x = -MAP_WIDTH / 2; x <= MAP_WIDTH / 2; x++) {
      Int3 pos = static_cast<Int3>(_cursor_position + Int2(x, y));
      pos[2] = camera_pos.get_z();
      if (map->is_tile_inside(pos)) {
        Int2 offset = center + Int2(x, y);
        TileID tile_id = map->get_tile(pos);
        _win->write(offset * scale, _TILE_GLYPH(tile_id));
        // Open area above either bedrock or nonempty tiles are drawn as '.'
        if (tile_id == TileID::EMPTY && (pos.get_k() == Map::BEDROCK_LEVEL ||
                                         map->get_tile(pos - Int3::unit_k()) != TileID::EMPTY)) {
          _win->write(offset * scale, L'.');
        }
      }
    }
  }

  for (const auto location : session.world.get_pool<Location>()) {
    if (location->get_position().get_k() == camera_pos.get_k()) {
      Int2 entity_viewpos =
          center + (static_cast<Int2>(location->get_position()) - _cursor_position);
      auto appearance = location->select<const Visible>()->get_appearance();
      _win->write(entity_viewpos * scale, _ENTITY_GLYPH(appearance));
    }
  }
  _win->highlight(center * scale);
  _win->refresh();
  _last_camera_pos = camera_pos;
}

} // namespace game::frontend
