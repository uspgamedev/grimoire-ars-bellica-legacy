
#ifndef GRIMOIRE_FRONTEND_STATUS_WINDOW_H_
#define GRIMOIRE_FRONTEND_STATUS_WINDOW_H_

#include "common/intn.h"

#include <memory>

namespace game::model {
class Session;
} // namespace game::model

namespace game::frontend {

class Window;

class StatusWindow final {
 public:
  StatusWindow();
  ~StatusWindow();

  StatusWindow(const StatusWindow &) = delete;
  StatusWindow &operator=(const StatusWindow &a) = delete;
  StatusWindow(StatusWindow &&) = delete;
  StatusWindow &operator=(StatusWindow &&) = delete;

  void adjust(Int2 screen_size);
  void show(const model::Session &session);

 private:
  std::unique_ptr<Window> _win;
};

} // namespace game::frontend

#endif // GRIMOIRE_FRONTEND_STATUS_WINDOW_H_
