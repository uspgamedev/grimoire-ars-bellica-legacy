
#include "config.h"

#ifdef GRIMOIRE_HAS_NCURSESW

#include <ncursesw/ncurses.h>

#else

#define NCURSES_WIDECHAR 1
#include <ncurses.h>
#undef NCURSES_WIDECHAR

#endif
