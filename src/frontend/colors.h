
#ifndef GRIMOIRE_FRONTEND_COLORS_H_
#define GRIMOIRE_FRONTEND_COLORS_H_

#include "frontend/curses.h"

#include <array>
#include <tuple>

namespace game::frontend {

// We use short here because that's what the curses API uses

enum class ColorPairID : short {
  DEFAULT = 1,
};

constexpr short TO_NATIVE(ColorPairID id) { return static_cast<short>(id); }

using ColorPair = std::tuple<short, short>;

inline const std::array<ColorPair, 8>& COLOR_PAIR_DEFS() {
  const static std::array<ColorPair, 8> DEFS = {{
    ColorPair{COLOR_WHITE, COLOR_BLACK}, /* actually reserved and thus unused */
    ColorPair{COLOR_WHITE, COLOR_BLACK},
    ColorPair{COLOR_WHITE, COLOR_BLACK},
    ColorPair{COLOR_WHITE, COLOR_BLACK},
    ColorPair{COLOR_WHITE, COLOR_BLACK},
    ColorPair{COLOR_WHITE, COLOR_BLACK},
    ColorPair{COLOR_WHITE, COLOR_BLACK},
    ColorPair{COLOR_WHITE, COLOR_BLACK},
  }};
  return DEFS;
}

} // namespace game::frontend

#endif // GRIMOIRE_FRONTEND_COLORS_H_

