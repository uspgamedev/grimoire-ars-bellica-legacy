
#include "frontend/log_window.h"
#include "frontend/window.h"

#include "frontend/layout.h"
#include "model/session.h"

namespace game::frontend {

namespace {
using model::Session;
using namespace layout;
} // unnamed namespace

LogWindow::LogWindow() : _win(new Window()) {}

// required by the incomplete type unique ptr
LogWindow::~LogWindow() = default;

void LogWindow::adjust(Int2 screen_size) {
  _win->reset(Int2(0, screen_size.get_y() - LOG_HEIGHT), Int2(screen_size.get_x(), LOG_HEIGHT));
}

void LogWindow::show(const Session &session) {
  _win->clear();
  _win->box();
  for (int i = 0; i < LOG_LINES; i++) {
    _win->write(Int2(1, LOG_HEIGHT - 2 - i), session.message_entry(-1 - i));
  }
  _win->refresh();
}

} // namespace game::frontend
