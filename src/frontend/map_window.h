
#ifndef GRIMOIRE_FRONTEND_MAP_WINDOW_H_
#define GRIMOIRE_FRONTEND_MAP_WINDOW_H_

#include "common/intn.h"

#include <memory>

namespace game::model {
class Session;
} // namespace game::model

namespace game::frontend {

class Window;

class MapWindow final {
 public:
  MapWindow();
  ~MapWindow();

  MapWindow(const MapWindow &) = delete;
  MapWindow &operator=(const MapWindow &a) = delete;
  MapWindow(MapWindow &&) = delete;
  MapWindow &operator=(MapWindow &&) = delete;

  void lock_cursor();
  void unlock_cursor();
  Int2 get_cursor_position() const;
  void set_cursor_position(const Int2 &position);

  void adjust(Int2 screen_size);
  void show(const model::Session &session);

 private:
  std::unique_ptr<Window> _win;
  Int2 _cursor_position{ 0, 0 };
  Int3 _last_camera_pos{ 0, 0, 0 };
  bool _cursor_locked{ false };
};

inline void MapWindow::lock_cursor() { _cursor_locked = true; }

inline void MapWindow::unlock_cursor() { _cursor_locked = false; }

inline Int2 MapWindow::get_cursor_position() const { return _cursor_position; }

inline void MapWindow::set_cursor_position(const Int2 &position) { _cursor_position = position; }

} // namespace game::frontend

#endif // GRIMOIRE_FRONTEND_MAP_WINDOW_H_
