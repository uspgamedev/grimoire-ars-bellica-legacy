
#ifndef GRIMOIRE_FRONTEND_UI_H_
#define GRIMOIRE_FRONTEND_UI_H_

#include "model/components/location.h"
#include "model/components/map.h"
#include "model/pool.h"

#include "common/intn.h"

#include <optional>

namespace game::model {
class Session;
}

namespace game::frontend {

class UI {
 public:
  /// Default constructor.
  /** Initializes curses.
   */
  UI();

  /// Default destructor.
  /** Finalizes curses.
   */
  ~UI();

  /// Deleted copy and move constructors and operators.
  UI(const UI &) = delete;
  UI &operator=(const UI &a) = delete;
  UI(UI &&) = delete;
  UI &operator=(UI &&) = delete;

  /// Flush all user input.
  /** Until the next call to `UI::present()`, there will be no user input buffered
   */
  void flush();

  void enable_input();
  void disable_input();

  /// Present UI and poll input.
  /** Calls to this method block the process until the user inputs anything.
   */
  void present(const model::Session &session);

  /// Enable entity navigation and report it if any ocurred.
  /** Returns an optional with the movement the user tried to perform with the controlled entity,
   ** or std::nullopt otherwise.
   */
  std::optional<Int3> entity_navigation();

  /// Enable cursor navigation and report whether any ocurred.
  /** Returns true if the user successfully moved the cursor around the map floor the
   ** player is currentrly at.
   */
  bool cursor_navigation(model::Handle<const model::components::Map> map,
                         model::Handle<const model::components::Location> player);

  /// Returns cursor curren on_screen position Int2
  Int2 cursor_position() const;

  /// Report whether the user tried to inspect something.
  bool inspect();

  /// Report whether the user tried to interact with something.
  bool interact();

  /// Report whether the user tried to pick up something.
  bool pickup();

  /// Report whether the user tried to quit the current state.
  bool quit();

  void emergency_exit();

 private:
  bool _valid{ false };
  struct Impl;
  Impl *_impl;
};

} // namespace game::frontend

#endif // GRIMOIRE_FRONTEND_UI_H_
