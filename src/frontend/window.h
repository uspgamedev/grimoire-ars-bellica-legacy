
#ifndef GRIMOIRE_FRONTEND_RAW_WINDOW_H_
#define GRIMOIRE_FRONTEND_RAW_WINDOW_H_

#include "common/intn.h"
#include "frontend/curses.h"

#include <string>

namespace game::model {
class Session;
}

namespace game::frontend {

class Window {
 public:
  Window();
  ~Window();

  Window(const Window &) = delete;
  Window &operator=(const Window &a) = delete;
  Window(Window &&) = delete;
  Window &operator=(Window &&) = delete;

  void clear();
  void resize(const Int2 &size);
  void move_to(const Int2 &pos);
  void reset(const Int2 &pos, const Int2 &size);
  void refresh();
  void box();
  void highlight(const Int2 &pos);
  void write(const Int2 &pos, const std::string &text);
  void write(const Int2 &pos, const std::wstring &text);
  void write(const Int2 &pos, wchar_t text);

 private:
  WINDOW *_win{ nullptr };
  Int2 _pos{ 0, 0 }, _size{ 1, 1 };
};

inline Window::~Window() {
  if (_win != nullptr) {
    delwin(_win);
  }
}

inline void Window::refresh() { ::wrefresh(_win); }

inline void Window::box() { ::box(_win, 0, 0); }

inline void Window::resize(const Int2 &size) { reset(size, _pos); }

inline void Window::move_to(const Int2 &pos) { reset(_size, pos); }

} // namespace game::frontend

#endif // GRIMOIRE_FRONTEND_RAW_WINDOW_H_
