
#include "frontend/status_window.h"
#include "frontend/window.h"

#include "frontend/layout.h"
#include "model/components/damageable.h"
#include "model/components/location.h"
#include "model/components/player.h"
#include "model/session.h"

namespace game::frontend {

namespace {
using model::Session;
using model::components::Damageable;
using model::components::Location;
using std::to_string;
using namespace layout;
} // unnamed namespace

StatusWindow::StatusWindow() : _win(new Window()) {}

// required by the incomplete type unique ptr
StatusWindow::~StatusWindow() = default;

void StatusWindow::adjust(Int2 screen_size) {
  _win->reset(Int2(screen_size.get_x() - STATUS_WIDTH, 0),
              Int2(STATUS_WIDTH, screen_size.get_y() - LOG_HEIGHT));
}

void StatusWindow::show(const Session &session) {
  _win->clear();
  _win->box();
  if (auto player = session.world.get_player()) {
    _win->write(Int2(1, 1), to_string(player->select<Damageable>()->get_hp()) + "/" +
                                to_string(player->select<Damageable>()->get_max_hp()));
    _win->write(Int2(1, 2), to_string(player->select<Location>()->get_position()));
  } else {
    _win->write(Int2(1, 1), L"< d e a d >");
  }
  _win->refresh();
}

} // namespace game::frontend
