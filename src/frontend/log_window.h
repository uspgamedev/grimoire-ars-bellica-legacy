
#ifndef GRIMOIRE_FRONTEND_LOG_WINDOW_H_
#define GRIMOIRE_FRONTEND_LOG_WINDOW_H_

#include "common/intn.h"

#include <memory>

namespace game::model {
class Session;
} // namespace game::model

namespace game::frontend {

class Window;

class LogWindow final {
 public:
  LogWindow();
  ~LogWindow();

  LogWindow(const LogWindow &) = delete;
  LogWindow &operator=(const LogWindow &a) = delete;
  LogWindow(LogWindow &&) = delete;
  LogWindow &operator=(LogWindow &&) = delete;

  void adjust(Int2 screen_size);
  void show(const model::Session &session);

 private:
  std::unique_ptr<Window> _win;
};

} // namespace game::frontend

#endif // GRIMOIRE_FRONTEND_LOG_WINDOW_H_
