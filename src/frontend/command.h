
#ifndef CODENAME_CURSED_COMMAND_H_
#define CODENAME_CURSED_COMMAND_H_

namespace game::frontend {

enum class Command : int {
  INVALID = 0,
  UP = 1,
  DOWN = 2,
  LEFT = 3,
  RIGHT = 4,
  UPLEFT = 5,
  DOWNLEFT = 6,
  UPRIGHT = 7,
  DOWNRIGHT = 8,
  INTERACT,
  INSPECT,
  QUIT,
  PICKUP
};
}

#endif // CODENAME_CURSED_COMMAND_H_
