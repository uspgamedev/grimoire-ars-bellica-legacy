
#ifndef GRIMOIRE_FRONTEND_LAYOUT_H_
#define GRIMOIRE_FRONTEND_LAYOUT_H_

namespace game::frontend::layout {

constexpr int LOG_HEIGHT = 8;
constexpr int LOG_LINES = LOG_HEIGHT - 2;
constexpr int STATUS_WIDTH = 42;
constexpr int MAP_WIDTH = 21;
constexpr int MAP_HEIGHT = 21;

} // namespace game::frontend::layout

#endif // GRIMOIRE_FRONTEND_LAYOUT_H_
