
#include "state/dead.h"

#include "frontend/ui.h"
#include "subsystemrack.h"

namespace game::state {

void Dead::on_enter(const Message & /* message */) { subsystemrack().ui.enable_input(); }

void Dead::on_leave() { subsystemrack().ui.disable_input(); }

StackOperation Dead::process() {
  if (subsystemrack().ui.quit()) {
    _log("Exiting game");
    return POP_STATE(Message{ { "quit", Argument{ true } } });
  }
  return CONTINUE_STATE();
}

} // namespace game::state
