
#ifndef GRIMOIRE_STATE_STACK_H_
#define GRIMOIRE_STATE_STACK_H_

#include "frontend/ui.h"
#include "state/base.h"
#include "state/statearguments.h"
#include "subsystemrack.h"

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <cstddef>

namespace game::state {

class Stack final {
 public:
  /// Main constructor.
  explicit Stack(SubsystemRack &subsystemrack);

  /// Deleted copy and move constructors and operators.
  Stack(const Stack &) = delete;
  Stack &operator=(const Stack &) = delete;
  Stack(Stack &&) = delete;
  Stack &operator=(Stack &&) = delete;

  /// Default destructor.
  ~Stack() = default;

  /// Register a state to the stack, so it can be pushed in the future.
  template <typename T>
  void register_state(const std::string &name);

  /// Process the state at the top of the stack.
  /** Returns true if the stack changed, meaning there's a new state to be processed as
   ** sooon as possible.
   */
  bool update();

  /// Tell whether there's any states left stacked.
  bool has_state() const;

  /// Push the state registered with the given name.
  /** Does nothing in case said state is already in the stack.
   */
  void push(const std::string &name, const Message &message = Message{});

  /// Pop the state on the top of the stack.
  void pop(const Message &message = Message{});

 private:
  void register_state_pointer(const std::string &name, Base *state);
  Base *current_state();
  SubsystemRack &_subsystemrack;
  std::unordered_map<std::string, std::unique_ptr<Base>> _registered_states;
  std::vector<std::string> _state_stack;
};

inline Stack::Stack(SubsystemRack &subsystemrack)
    : _subsystemrack(subsystemrack), _registered_states(), _state_stack() {}

template <typename T>
inline void Stack::register_state(const std::string &name) {
  auto state = static_cast<Base *>(new T(_subsystemrack));
  register_state_pointer(name, state);
}

inline bool Stack::has_state() const { return !_state_stack.empty(); }

inline void Stack::register_state_pointer(const std::string &name, Base *state) {
  _registered_states[name] = std::unique_ptr<Base>(state);
}

inline Base *Stack::current_state() {
  if (!_state_stack.empty()) return _registered_states[_state_stack.back()].get();
  return nullptr;
}

} // namespace game::state

#endif // GRIMOIRE_STATE_STACK_H_
