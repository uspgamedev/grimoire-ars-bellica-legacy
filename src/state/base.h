
#ifndef GRIMOIRE_STATE_BASE_H_
#define GRIMOIRE_STATE_BASE_H_

#include "common/log.h"
#include "state/stackop.h"
#include "state/statearguments.h"
#include "subsystemrack.h"

#include <memory>
#include <random>

namespace game::state {

class Stack;

class Base {
 public:
  Base(const Base &) = delete;
  Base &operator=(const Base &) = delete;
  Base(Base &&) = delete;
  Base &operator=(Base &&) = delete;
  virtual ~Base() = default;
  virtual void on_enter(const Message &message);
  virtual void on_leave();
  virtual void on_resume(const Message &message);
  virtual void on_suspend();
  virtual StackOperation process() = 0;

 protected:
  Base(SubsystemRack &subsystemrack, std::string logger_name);
  SubsystemRack &subsystemrack();
  const game::Log _log;

 private:
  SubsystemRack &_subsystemrack;
};

inline void Base::on_enter(const Message & /*message*/) {}
inline void Base::on_leave() {}
inline void Base::on_resume(const Message & /*message*/) {}
inline void Base::on_suspend() {}

inline Base::Base(SubsystemRack &subsystemrack, std::string logger_name)
    : _log(std::move(logger_name)), _subsystemrack(subsystemrack) {}

inline SubsystemRack &Base::subsystemrack() { return _subsystemrack; }

} // namespace game::state

#endif // GRIMOIRE_STATE_BASE_H_
