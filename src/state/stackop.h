
#ifndef GRIMOIRE_STATE_STACKOP_H_
#define GRIMOIRE_STATE_STACKOP_H_

#include "state/statearguments.h"

#include <memory>
#include <string>
#include <tuple>
#include <utility>

namespace game::state {

enum class StackOperationType : unsigned char { CONTINUE, PUSH, POP, SWITCH };

using StackOperation = std::tuple<StackOperationType, std::string, Message>;

inline StackOperation CONTINUE_STATE() {
  return std::make_tuple(StackOperationType::CONTINUE, "", Message{});
}

inline StackOperation POP_STATE() {
  return std::make_tuple(StackOperationType::POP, "", Message{});
}

inline StackOperation POP_STATE(Message &&message) {
  return std::make_tuple(StackOperationType::POP, "", std::move(message));
}

inline StackOperation PUSH_STATE(const std::string &name) {
  return std::make_tuple(StackOperationType::PUSH, name, Message{});
}

inline StackOperation PUSH_STATE(const std::string &name, Message &&message) {
  return std::make_tuple(StackOperationType::PUSH, name, std::move(message));
}

inline StackOperation SWITCH_STATE(const std::string &name) {
  return std::make_tuple(StackOperationType::SWITCH, name, Message{});
}

inline StackOperation SWITCH_STATE(const std::string &name, Message &&message) {
  return std::make_tuple(StackOperationType::SWITCH, name, std::move(message));
}

} // namespace game::state

#endif // GRIMOIRE_STATE_STACKOP_H_
