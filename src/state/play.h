
#ifndef GRIMOIRE_PLAYSTATE_H_
#define GRIMOIRE_PLAYSTATE_H_

#include "state/base.h"
#include "state/stackop.h"

#include <vector>

namespace game::state {

class Play : public Base {
 public:
  explicit Play(SubsystemRack &subsystemrack);
  void on_enter(const Message & /*message*/) override;
  void on_resume(const Message & /*message*/) override;
  StackOperation process() override;

 private:
  bool _leave = false;
};

inline Play::Play(SubsystemRack &subsystemrack) : Base(subsystemrack, "play") {}

inline void Play::on_enter(const Message & /*message*/) { _leave = false; }

} // namespace game::state

#endif // GRIMOIRE_PLAYSTATE_H_
