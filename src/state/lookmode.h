
#ifndef GRIMOIRE_LOOKMODE_H_
#define GRIMOIRE_LOOKMODE_H_

#include "state/base.h"
#include "state/stackop.h"

namespace game::state {

class LookMode : public Base {
 public:
  explicit LookMode(SubsystemRack &subsystemrack);
  StackOperation process() override;
};

inline LookMode::LookMode(SubsystemRack &subsystemrack) : Base(subsystemrack, "look_mode") {}

} // namespace game::state

#endif // GRIMOIRE_LOOKMODE_H_
