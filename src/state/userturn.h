
#ifndef GRIMOIRE_USERTURN_H_
#define GRIMOIRE_USERTURN_H_

#include "frontend/ui.h"
#include "state/base.h"
#include "state/stackop.h"

namespace game::model::components {
class Location;
}

namespace game::state {

class UserTurn : public Base {
 public:
  explicit UserTurn(SubsystemRack &subsystemrack);
  void on_enter(const Message & /*message*/) override;
  void on_leave() override;
  void on_resume(const Message &message) override;
  StackOperation process() override;

 private:
  StackOperation movement_actions(const Int3 &movement);
  StackOperation interaction_actions();
  StackOperation pickup_actions();
  bool _leave = false;
  size_t _turn_count{ 0 };
};

inline UserTurn::UserTurn(SubsystemRack &subsystemrack) : Base(subsystemrack, "user_turn") {}

} // namespace game::state

#endif // GRIMOIRE_USERTURN_H_
