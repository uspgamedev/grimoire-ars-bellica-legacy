
#ifndef GRIMOIRE_STATE_ARGUMENTS_H_
#define GRIMOIRE_STATE_ARGUMENTS_H_

#include "model/identity.h"
#include "model/pool.h"

#include <memory>
#include <string>
#include <unordered_map>
#include <variant>

namespace game::state {
 
struct Unused {};
struct EntitySelectionArg {
  model::ID _selected_target_id;
};

using Argument = std::variant<Unused, EntitySelectionArg, bool>;
using Message = std::unordered_map<std::string, Argument>;

} // namespace game::state

#endif // GRIMOIRE_STATE_STACKOP_H_
