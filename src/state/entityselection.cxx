
#include "state/entityselection.h"

#include "common/intn.h"
#include "frontend/ui.h"
#include "model/components/location.h"
#include "model/components/map.h"
#include "model/components/player.h"
#include "subsystemrack.h"

namespace game::state {

namespace {

using model::components::Location;

} // unnamed namespace

StackOperation EntitySelection::process() {
  auto &[session, ui] = subsystemrack();
  auto player = session.world.get_component<Location>(session.world.get_player()->get_id());
  Int2 cursor_scr = ui.cursor_position();
  Int3 cursoworld = static_cast<Int3>(cursor_scr) + Int3(0, 0, player->get_position().get_z());

  _log("Cursor at % % %", cursoworld.get_x(), cursoworld.get_y(), cursoworld.get_z());

  ui.cursor_navigation(session.world.get_map().to_const(), player.to_const());

  if (ui.quit()) {
    _log("Left quitting");
    session.message(L"Canceled selection");
    return POP_STATE();
  }
  if (ui.interact()) {
    auto maybe_target_id = session.world.get_map()->get_entity_id_at(cursoworld);

    _log("Left selecting");

    if (maybe_target_id) {
      auto target_id = maybe_target_id.value();
      _log("Selected % %", target_id, target_id == player->get_id() ? "(player)" : "");
      return POP_STATE(Message{ { "target", Argument{ EntitySelectionArg{ target_id } } } });
    }
    _log("Selected %", "floor");
    session.message(L"Invalid selection");
    return POP_STATE();
  }
  return CONTINUE_STATE();
}

void EntitySelection::on_enter(const Message & /*message*/) {
  _log("Entering selection mode");
  auto &[session, _] = subsystemrack();
  session.message(L"Please select a target");
}

} // namespace game::state
