
#include "state/play.h"

#include "model/round_loop.h"
#include "state/stackop.h"
#include "subsystemrack.h"

namespace game::state {

void Play::on_resume(const Message &message) {
  try {
    bool quit = std::get<bool>(message.at("quit"));
    if (quit) _leave = true;
  } catch (...) {
  }
}

StackOperation Play::process() {
  using Status = model::RoundLoop::Status;
  if (_leave) return POP_STATE();
  switch (subsystemrack().session.round_loop()) {
    case Status::USER_ACTION_REQUESTED:
      return PUSH_STATE("USER_TURN");
    case Status::NO_PLAYER:
      return PUSH_STATE("DEAD");
    case Status::REPORT:
      return CONTINUE_STATE();
    default:
      break;
  }
  return CONTINUE_STATE();
}

} // namespace game::state
