
#ifndef GRIMOIRE_ENTITY_SELECTION_H_
#define GRIMOIRE_ENTITY_SELECTION_H_

#include "state/base.h"
#include "state/stackop.h"

namespace game::state {

class EntitySelection : public Base {
 public:
  explicit EntitySelection(SubsystemRack &subsystemrack);

  void on_enter(const Message &message) override;
  StackOperation process() override;
};

inline EntitySelection::EntitySelection(SubsystemRack &subsystemrack)
    : Base(subsystemrack, "entity_selection") {}

} // namespace game::state

#endif // GRIMOIRE_USERTURN_H_
