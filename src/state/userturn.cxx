
#include "state/userturn.h"

#include "common/intn.h"
#include "frontend/ui.h"
#include "model/action.h"
#include "model/appearance.h"
#include "model/components/attacker.h"
#include "model/components/damageable.h"
#include "model/components/location.h"
#include "model/components/manipulator.h"
#include "model/components/map.h"
#include "model/components/openable.h"
#include "model/components/player.h"
#include "model/components/visible.h"
#include "subsystemrack.h"

#include <exception>

namespace game::state {

namespace {

using model::nullhandle;
using model::actions::MOVE;
using model::actions::PICK_UP;
using model::actions::TOGGLE_DOOR;
using model::components::Attacker;
using model::components::Damageable;
using model::components::Location;
using model::components::Manipulator;
using model::components::Openable;

} // unnamed namespace

void UserTurn::on_enter(const Message & /* message */) {
  _leave = false;
  subsystemrack().ui.enable_input();
}

void UserTurn::on_leave() { subsystemrack().ui.disable_input(); }

void UserTurn::on_resume(const Message &message) {
  _log("Resuming User Turn");
  try {
    auto &session = subsystemrack().session;
    auto entity_selection = std::get<EntitySelectionArg>(message.at("target"));
    auto target_id = entity_selection._selected_target_id;
    _log("Received message: %", target_id);
    auto player = session.world.get_player();
    auto attacker = player->select<Attacker>();
    auto manipulator = player->select<Manipulator>();
    auto damageable = session.world.get_component<Damageable>(target_id);
    auto openable = session.world.get_component<Openable>(target_id);
    if (manipulator != nullhandle && openable != nullhandle) {
      session.message(L"Opening target: " + std::to_wstring(target_id));
      player->set_next_action(TOGGLE_DOOR(target_id));
    } else if (attacker != nullhandle && damageable != nullhandle) {
      session.message(L"Attacking target: " + std::to_wstring(target_id));
      player->set_next_action(attacker->get_attack_action(target_id));
    }
    _leave = true;
  } catch (std::exception &e) {
    _log("Didn't get a valid message: %", e.what());
  }
}

StackOperation UserTurn::process() {
  if (_leave) return POP_STATE();
  auto &[session, ui] = subsystemrack();
  _turn_count++;
  _log("Starting turn #%", _turn_count);

  if (auto movement_opt = ui.entity_navigation()) return movement_actions(*movement_opt);

  if (ui.inspect()) return PUSH_STATE("LOOK_MODE");

  if (ui.quit()) {
    _log("Exiting game");
    return POP_STATE(Message{ { "quit", Argument{ true } } });
  }

  if (ui.interact()) return interaction_actions();

  if (ui.pickup()) return pickup_actions();

  return CONTINUE_STATE();
}

StackOperation UserTurn::movement_actions(const Int3 &movement) {
  auto &session = subsystemrack().session;
  auto player = session.world.get_player();
  auto other_ids = session.world.get_map()->get_entity_ids_at(
      player->select<Location>()->get_position() + static_cast<Int3>(movement));
  for (auto other_id : other_ids) {
    if (!session.world.get_component<Location>(other_id)->is_passable()) {
      if (auto other_openable = session.world.get_component<Openable>(other_id);
          other_openable != nullhandle) {
        player->set_next_action(TOGGLE_DOOR(other_openable->get_id()));
        return POP_STATE();
      }
      if (auto other_damageable = session.world.get_component<Damageable>(other_id);
          other_damageable != nullhandle) {
        player->set_next_action(
            player->select<Attacker>()->get_attack_action(other_damageable->get_id()));
        return POP_STATE();
      }
      return CONTINUE_STATE();
    }
  }
  auto dir = int2_to_dir(static_cast<Int2>(movement));
  player->set_next_action(MOVE(dir));
  return POP_STATE();
}

StackOperation UserTurn::interaction_actions() {
  auto &session = subsystemrack().session;
  auto player_manipulator = session.world.get_player()->select<Manipulator>();
  if (player_manipulator.is_valid()) return PUSH_STATE("SELECT_MODE");
  session.message(L"You don't have a way to interact with things.");
  return CONTINUE_STATE();
}

StackOperation UserTurn::pickup_actions() {
  auto &session = subsystemrack().session;
  auto player = session.world.get_player();
  auto player_manipulator = player->select<Manipulator>();
  player->set_next_action(PICK_UP());
  _log("picking up");

  if (player_manipulator.is_valid()) return POP_STATE();
  session.message(L"You don't have a way to interact with things.");
  return CONTINUE_STATE();
}

} // namespace game::state
