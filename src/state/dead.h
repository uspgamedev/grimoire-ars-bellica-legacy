
#ifndef GRIMOIRE_DEAD_H_
#define GRIMOIRE_DEAD_H_

#include "frontend/ui.h"
#include "state/base.h"
#include "state/stackop.h"

namespace game::state {

class Dead : public Base {
 public:
  explicit Dead(SubsystemRack &subsystemrack);
  void on_enter(const Message & /*message*/) override;
  void on_leave() override;
  StackOperation process() override;
};

inline Dead::Dead(SubsystemRack &subsystemrack) : Base(subsystemrack, "dead") {}

} // namespace game::state

#endif // GRIMOIRE_DEAD_H_
