
#include "state/stack.h"
#include "state/stackop.h"

#include "common/log.h"

#include <exception>

namespace game::state {

namespace {
using std::string;
} // unnamed namespace

void Stack::push(const string &name, const Message &message) {
  if (_registered_states.find(name) == _registered_states.end()) return;
  for (const string &state_name : _state_stack)
    if (state_name == name) return;
  if (!_state_stack.empty()) current_state()->on_suspend();
  _state_stack.push_back(name);
  current_state()->on_enter(message);
}

void Stack::pop(const Message &message) {
  if (_state_stack.empty()) throw std::logic_error("Cannot pop from empty stack");
  current_state()->on_leave();
  _state_stack.pop_back();
  if (!_state_stack.empty()) {
    current_state()->on_resume(message);
  }
}

bool Stack::update() {
  if (_state_stack.empty()) return false;
  bool changed = true;
  const auto &[op, name, message] = current_state()->process();
  switch (op) {
    case StackOperationType::CONTINUE:
      changed = false;
      break;
    case StackOperationType::PUSH:
      push(name, message);
      break;
    case StackOperationType::POP:
      pop(message);
      break;
    case StackOperationType::SWITCH:
      pop();
      push(name, message);
      break;
  }
  return changed;
}

} // namespace game::state
