
#include "state/lookmode.h"

#include "common/intn.h"
#include "frontend/ui.h"
#include "model/components/location.h"
#include "model/components/player.h"
#include "subsystemrack.h"

namespace game::state {

StackOperation LookMode::process() {
  auto &ui = subsystemrack().ui;
  const auto &session = subsystemrack().session;
  auto player = session.world.get_component<model::components::Location>(
      session.world.get_player()->get_id());
  ui.cursor_navigation(session.world.get_map(), player);
  if (ui.quit()) {
    _log("Exiting lookmode");
    return POP_STATE();
  }
  return CONTINUE_STATE();
}

} // namespace game::state
