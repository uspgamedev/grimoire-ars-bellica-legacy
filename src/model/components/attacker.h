
#ifndef GRIMOIRE_MODEL_COMPONENTS_ATTACKER_H_
#define GRIMOIRE_MODEL_COMPONENTS_ATTACKER_H_

#include "model/component.h"

#include "model/action.h"
#include "model/schema.h"

#include <algorithm>
#include <tuple>

namespace game::model::components {

class Attacker final : public Component {
 public:
  Attacker(World &the_world, ID id, size_t power, size_t skill);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _power, _skill }; }

  // getter & setter
  size_t get_power() const;
  void set_power(size_t power);
  size_t get_skill() const;
  void set_skill(size_t skill);

  Action get_attack_action(ID target_id) const;

 private:
  size_t _power;
  size_t _skill;
};

inline Attacker::Attacker(World &the_world, ID id, size_t power, size_t skill)
    : Component(the_world, id), _power(power), _skill(skill) {}

inline size_t Attacker::get_power() const { return _power; }

inline void Attacker::set_power(size_t power) { _power = power; }

inline size_t Attacker::get_skill() const { return _skill; }

inline void Attacker::set_skill(size_t skill) { _skill = skill; }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_ATTACKER_H_
