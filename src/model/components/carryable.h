
#ifndef GRIMOIRE_MODEL_COMPONENTS_CARRYABLE_H_
#define GRIMOIRE_MODEL_COMPONENTS_CARRYABLE_H_

#include "model/component.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Carryable final : public Component {
 public:
  Carryable(World &the_world, ID id, size_t weight);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _weight }; }
  size_t get_weight() const;

 private:
  size_t _weight;
};

inline Carryable::Carryable(World &the_world, ID id, size_t weight)
    : Component(the_world, id), _weight(weight) {}

inline size_t Carryable::get_weight() const { return _weight; }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_CARRYABLE_H_
