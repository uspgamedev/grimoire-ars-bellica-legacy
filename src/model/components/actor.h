
#ifndef GRIMOIRE_MODEL_COMPONENTS_ACTOR_H_
#define GRIMOIRE_MODEL_COMPONENTS_ACTOR_H_

#include "model/component.h"
#include "model/schema.h"

#include <algorithm>
#include <tuple>

namespace game::model::components {

class Actor final : public Component {
 public:
  Actor(World &the_world, ID id, size_t speed, size_t energy = 0);

  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _speed, _energy }; }

  void tick();
  bool is_ready() const;
  void spend_energy(size_t amount);

  static constexpr size_t MAX_ENERGY = 100;

 private:
  size_t _speed{ 0 };
  size_t _energy{ 0 };
};

inline Actor::Actor(World &the_world, ID id, size_t speed, size_t energy)
    : Component(the_world, id), _speed(speed), _energy(energy) {}

inline void Actor::tick() { _energy = std::min(MAX_ENERGY, _energy + _speed); }

inline bool Actor::is_ready() const { return _energy >= MAX_ENERGY; }

inline void Actor::spend_energy(size_t amount) { _energy -= std::min(amount, _energy); }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_ACTOR_H_
