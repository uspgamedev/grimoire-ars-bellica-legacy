
#include "model/components/agent.h"

#include "model/schema_list.h"

#include <random>

namespace game::model::components {

namespace {

Action random_walk() {
  static std::mt19937 rng{ 9238775 };
  static std::uniform_int_distribution<size_t> dir_idx{ 1, 8 };
  auto idx = dir_idx(rng);
  return actions::MOVE(static_cast<Direction>(idx));
}

Handle<const Player> visible_player(Handle<const Seer> seer) {
  for (auto other_entity_id : seer->visible_ids()) {
    auto player = seer->world.get_component<const Player>(other_entity_id);
    if (player != nullhandle) return player;
  }
  return nullhandle;
}

} // unnamed namespace

Action Agent::decide_action() const {
  // Needs to see to chase anything
  if (auto seer = select<Seer>(); seer != nullhandle) {
    // Only chase player entities
    auto player = visible_player(seer);
    if (player != nullhandle) {
      auto this_location = select<Location>();
      auto player_location = player->select<Location>();
      if (this_location != nullhandle && player_location != nullhandle) {
        Int3 diff = (player_location->get_position() - this_location->get_position());
        auto attacker = select<Attacker>();
        if (attacker != nullhandle && diff.size() <= 1)
          return attacker->get_attack_action(player->get_id());
        return actions::MOVE(int2_to_dir(static_cast<Int2>(diff.unit())));
      }
    }
  }
  return random_walk();
}

} // namespace game::model::components
