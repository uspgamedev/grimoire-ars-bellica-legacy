
#ifndef GRIMOIRE_MODEL_COMPONENTS_POSITION_H_
#define GRIMOIRE_MODEL_COMPONENTS_POSITION_H_

#include "common/intn.h"
#include "model/component.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Location final : public Component {
 public:
  Location(World &the_world, ID id, Int3 position, bool passable);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _position, _passable }; }

  // Position
  const Int3 get_position() const;
  void set_position(const Int3 &new_position);
  bool move(const Int3 &movement);
  size_t squared_distance_to(Handle<const Location> other_location) const;
  bool is_within(Handle<const Location> other_location, size_t distance) const;

  // Passable
  bool is_passable() const;
  void set_passable(bool new_passable);

 private:
  Int3 _position;
  bool _passable;
};

inline Location::Location(World &the_world, ID id, Int3 position, bool passable)
    : Component(the_world, id), _position(position), _passable(passable) {}

inline const Int3 Location::get_position() const { return _position; }

inline void Location::set_position(const Int3 &new_position) { _position = new_position; }

inline bool Location::is_passable() const { return _passable; }

inline void Location::set_passable(bool new_passable) { _passable = new_passable; }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_POSITION_H_
