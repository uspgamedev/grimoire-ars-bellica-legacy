
#ifndef GRIMOIRE_MODEL_COMPONENTS_NAMED_H_
#define GRIMOIRE_MODEL_COMPONENTS_NAMED_H_

#include "model/component.h"
#include "model/schema.h"

#include <string>
#include <tuple>
#include <utility>

namespace game::model::components {

class Named final : public Component {
 public:
  Named(World &the_world, ID id, std::string name);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple<std::string>(_name); }

 private:
  std::string _name;
};

inline Named::Named(World &the_world, ID id, std::string name)
    : Component(the_world, id), _name(std::move(name)) {}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_NAMED_H_
