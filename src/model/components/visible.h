
#ifndef GRIMOIRE_MODEL_COMPONENTS_VISIBLE_H_
#define GRIMOIRE_MODEL_COMPONENTS_VISIBLE_H_

#include "model/appearance.h"
#include "model/component.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Visible final : public Component {
 public:
  Visible(World &the_world, ID id, Appearance look = Appearance::INDESCRIBABLE);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _appearance }; }
  // Appearance
  Appearance get_appearance() const;
  void set_appearance(Appearance new_appearance);

 private:
  Appearance _appearance;
};

inline Visible::Visible(World &the_world, ID id, Appearance look)
    : Component(the_world, id), _appearance(look) {}

inline Appearance Visible::get_appearance() const { return _appearance; }
inline void Visible::set_appearance(Appearance new_appearance) { _appearance = new_appearance; }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_VISIBLE_H_
