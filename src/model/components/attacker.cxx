
#include "model/components/attacker.h"

#include "model/components/damageable.h"

namespace game::model::components {

Action Attacker::get_attack_action(ID target_id) const {
  auto power = this->_power;
  return Action{ 100 / _skill, [power, target_id](World &the_world, ID /*actor_id*/) -> bool {
                  auto damageable = the_world.get_component<Damageable>(target_id);
                  if (damageable != nullhandle) {
                    damageable->take_damage(power);
                    return true;
                  }
                  // target cannot be damaged
                  return false;
                } };
}

} // namespace game::model::components
