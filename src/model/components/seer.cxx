
#include "model/components/seer.h"
#include "model/components/location.h"
#include "model/components/visible.h"

namespace game::model::components {

std::vector<ID> Seer::visible_ids() const {
  const auto &bodies = this->world.get_pool<Location>();
  const auto &visibles = this->world.get_pool<Visible>();

  auto my_location = bodies.at(this->get_id());
  if (!my_location) return std::vector<ID>();

  std::vector<ID> visible_list;
  for (const auto &location : bodies)
    if ((location->get_position() - my_location->get_position()).size() <= _range)
      if (visibles.at(location->get_id())) {
        visible_list.push_back(location->get_id());
      }
  return visible_list;
}

} // namespace game::model::components
