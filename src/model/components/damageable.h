
#ifndef GRIMOIRE_MODEL_COMPONENTS_DAMAGEABLE_H_
#define GRIMOIRE_MODEL_COMPONENTS_DAMAGEABLE_H_

#include "model/component.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Damageable final : public Component {
 public:
  Damageable(World &the_world, ID id, size_t max_hp, size_t damage);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _max_hp, _damage }; }

  // getter & setter
  size_t get_max_hp() const;
  void set_max_hp(size_t max_hp);
  size_t get_hp() const;

  void take_damage(size_t amount);
  bool is_dead() const;

 private:
  void _die();
  size_t _max_hp;
  size_t _damage;
};

inline Damageable::Damageable(World &the_world, ID id, size_t max_hp, size_t damage)
    : Component(the_world, id), _max_hp(max_hp), _damage(damage) {}

inline size_t Damageable::get_max_hp() const { return _max_hp; }

inline size_t Damageable::get_hp() const { return _max_hp - _damage; }

inline bool Damageable::is_dead() const { return _damage >= _max_hp; }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_DAMAGEABLE_H_
