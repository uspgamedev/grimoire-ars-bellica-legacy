
#ifndef GRIMOIRE_MODEL_COMPONENTS_SEER_H_
#define GRIMOIRE_MODEL_COMPONENTS_SEER_H_

#include "model/component.h"
#include "model/schema.h"

#include <iostream>
#include <tuple>
#include <vector>

namespace game::model::components {

class Seer final : public Component {
 public:
  Seer(World &the_world, ID id, size_t range);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple<size_t>{ _range }; }

  /// returns the set of all entities with a body and appearance within range.
  std::vector<ID> visible_ids() const;

  template <typename Component>
  std::vector<Handle<const Component>> visible() const;

 private:
  size_t _range;
};

inline Seer::Seer(World &the_world, ID id, size_t range)
    : Component(the_world, id), _range(range) {}

template <typename Component>
inline std::vector<Handle<const Component>> Seer::visible() const {
  std::vector<Handle<const Component>> with_component;

  for (auto &entity_id : visible_ids())
    if (Handle<const Component> handler = this->world.get_component<const Component>(entity_id))
      with_component.push_back(handler);

  return with_component;
}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_SEER_H_
