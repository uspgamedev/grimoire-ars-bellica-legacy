
#include "model/components/container.h"
#include "model/components/location.h"

namespace game::model::components {

bool Container::has_item(ID id) const {
  for (const auto &item_id : _held_items)
    if (item_id == id) return true;
  return false;
}

bool Container::add_item(ID id) {
  if (has_item(id)) return false;

  if (this->world.get_component<Location>(id)) this->world.destroy_component<Location>(id);
  _held_items.push_back(id);

  return true;
}

bool Container::drop_item(ID id) {
  auto location = this->world.get_component<Location>(get_id());
  if (!has_item(id)) return false;
  if (location) {
    this->world.register_component<Location>(id, Int3{ 0, 0, 0 }, true);
    this->world.get_component<Location>(id)->set_position(location->get_position());
  }

  int index = 0;
  for (auto curr_id : _held_items) {
    if (curr_id == id) break;
    index++;
  }

  _held_items.erase(_held_items.begin() + index);
  return true;
}

} // namespace game::model::components
