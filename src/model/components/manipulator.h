
#ifndef GRIMOIRE_MODEL_COMPONENTS_MANIPULATOR_H_
#define GRIMOIRE_MODEL_COMPONENTS_MANIPULATOR_H_

#include "model/component.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Manipulator final : public Component {
 public:
  Manipulator(World &the_world, ID id, size_t reach);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _reach }; }

  // getter & setter
  size_t get_reach() const;
  void set_reach(size_t reach);

 private:
  size_t _reach;
};

inline Manipulator::Manipulator(World &the_world, ID id, size_t reach)
    : Component(the_world, id), _reach(reach) {}

inline size_t Manipulator::get_reach() const { return _reach; }
inline void Manipulator::set_reach(size_t reach) { _reach = reach; }

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_MANIPULATOR_H_
