
#ifndef GRIMOIRE_MODEL_MAP_H_
#define GRIMOIRE_MODEL_MAP_H_

#include "model/component.h"
#include "model/components/location.h"
#include "model/identity.h"
#include "model/pool.h"
#include "model/tile_map.h"

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include <cstddef>
#include <cstdint>

namespace game::model {
class Session;
}

namespace game::model::components {

class Map final : public Component {
 public:
  // Constructor
  Map(World &the_world, ID id, const std::string &tile_data, size_t w, size_t h, size_t d);

  // Useful macro value
  static constexpr size_t BEDROCK_LEVEL{ 0 };

  // Schema/Tuple converter
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const {
    return std::tuple{ _tilemap.get_tile_data(), get_width(), get_height(), get_depth() };
  }

  // Public Methods
  size_t get_width() const;
  size_t get_height() const;
  size_t get_depth() const;
  bool is_tile_inside(const Int3 &pos) const;
  bool is_tile_passable(const Int3 &pos) const;
  TileMap::TileID get_tile(const Int3 &pos) const;
  void set_tile(const Int3 &pos, TileMap::TileID tile_id);
  std::optional<ID> get_entity_id_at(const Int3 &pos) const;
  std::vector<ID> get_entity_ids_at(const Int3 &pos) const;

 private:
  TileMap _tilemap;
};

inline size_t Map::get_width() const { return _tilemap.get_width(); }
inline size_t Map::get_height() const { return _tilemap.get_height(); }
inline size_t Map::get_depth() const { return _tilemap.get_depth(); }

inline bool Map::is_tile_inside(const Int3 &pos) const { return _tilemap.is_tile_inside(pos); }

inline TileMap::TileID Map::get_tile(const Int3 &pos) const { return _tilemap.get_tile(pos); }

inline void Map::set_tile(const Int3 &pos, TileMap::TileID tile_id) {
  _tilemap.set_tile(pos, tile_id);
}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_MAP_H_
