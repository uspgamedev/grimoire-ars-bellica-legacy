
#include "model/components/map.h"

#include "common/log.h"
#include "model/components/location.h"
#include "model/pool.h"
#include "model/session.h"
#include "model/tile_map.h"
#include "model/world.h"

#include <string>

namespace game::model::components {

namespace {
using std::nullopt;
using std::optional;
} // unnamed namespace

Map::Map(World &the_world, ID id, const std::string &tile_data, size_t w, size_t h, size_t d)
    : Component(the_world, id), _tilemap(tile_data, w, h, d) {}

bool Map::is_tile_passable(const Int3 &pos) const {
  if (get_tile(pos) != TileMap::TileID::EMPTY) return false;
  auto ids = get_entity_ids_at(pos);
  for (auto id : ids)
    if (!this->world.get_component<Location>(id)->is_passable()) return false;
  return true;
}

optional<ID> Map::get_entity_id_at(const Int3 &pos) const {
  auto ids = get_entity_ids_at(pos);
  if (ids.empty()) return nullopt;
  return ids.front();
}

std::vector<ID> Map::get_entity_ids_at(const Int3 &pos) const {
  std::vector<ID> list;
  for (auto location : this->world.get_pool<Location>()) {
    if (location->get_position() == pos) list.push_back(location->get_id());
  }
  return list;
}

} // namespace game::model::components
