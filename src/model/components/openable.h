
#ifndef GRIMOIRE_MODEL_COMPONENTS_OPENABLE_H_
#define GRIMOIRE_MODEL_COMPONENTS_OPENABLE_H_

/* Interactable Component should eventually replace Openable */

#include "model/component.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Openable final : public Component {
 public:
  Openable(World &the_world, ID id, bool open, Appearance opened_appearance,
           Appearance closed_appearance);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const {
    return std::tuple{ _open, _opened_appearance, _closed_appearance };
  }
  bool is_open() const;
  void toggle_open();
  Appearance get_appearence() const;

 private:
  bool _open;

  Appearance _opened_appearance;
  Appearance _closed_appearance;
};

inline Openable::Openable(World &the_world, ID id, bool open, Appearance opened_appearance,
                          Appearance closed_appearance)
    : Component(the_world, id), _open(open), _opened_appearance(opened_appearance),
      _closed_appearance(closed_appearance) {}

inline bool Openable::is_open() const { return _open; }

inline void Openable::toggle_open() { _open = !_open; }

inline Appearance Openable::get_appearence() const {
  return _open ? _opened_appearance : _closed_appearance;
}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_OPENABLE_H_
