
#include "model/components/damageable.h"

#include "model/schema_list.h"

namespace game::model::components {

void Damageable::set_max_hp(size_t max_hp) {
  _max_hp = max_hp;
  if (is_dead()) _die();
}

void Damageable::take_damage(size_t amount) {
  _damage += amount;
  if (is_dead()) _die();
}

void Damageable::_die() {
  const auto &id = get_id();

  this->world.destroy_component<Actor>(id);
  this->world.destroy_component<Agent>(id);
  this->world.destroy_component<Player>(id);
  this->world.destroy_component<Damageable>(id);
}

} // namespace game::model::components
