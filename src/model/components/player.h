
#ifndef GRIMOIRE_MODEL_COMPONENTS_PLAYER_H_
#define GRIMOIRE_MODEL_COMPONENTS_PLAYER_H_

#include "model/component.h"

#include "model/action.h"
#include "model/schema.h"

#include <algorithm>
#include <tuple>

namespace game::model::components {

class Player final : public Component {
 public:
  Player(World &the_world, ID id);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{}; }

  void set_next_action(Action action);
  bool has_next_action() const;
  Action pop_next_action();

 private:
  Action _next_action{};
};

inline Player::Player(World &the_world, ID id) : Component(the_world, id) {}

inline void Player::set_next_action(Action action) { _next_action = std::move(action); }

inline bool Player::has_next_action() const { return static_cast<bool>(_next_action.execute); }

inline Action Player::pop_next_action() {
  Action action;
  std::swap(_next_action, action);
  return action;
}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_PLAYER_H_
