
#ifndef GRIMOIRE_MODEL_COMPONENTS_CONTAINER_H_
#define GRIMOIRE_MODEL_COMPONENTS_CONTAINER_H_

#include "model/component.h"
#include "model/schema.h"

#include <set>
#include <tuple>

namespace game::model::components {

class Container final : public Component {
 public:
  Container(World &the_world, ID id, IDList inventory);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{ _held_items }; }

  bool has_item(ID id) const;
  bool add_item(ID id);
  bool drop_item(ID id);

 private:
  IDList _held_items;
};

inline Container::Container(World &the_world, ID id, IDList inventory)
    : Component(the_world, id), _held_items(std::move(inventory)) {}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_CONTAINER_H_
