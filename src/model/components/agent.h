
#ifndef GRIMOIRE_MODEL_COMPONENTS_AGENT_H_
#define GRIMOIRE_MODEL_COMPONENTS_AGENT_H_

#include "model/component.h"

#include "model/action.h"
#include "model/schema.h"

#include <tuple>

namespace game::model::components {

class Agent final : public Component {
 public:
  Agent(World &the_world, ID id);
  static const BaseSchema &schema();
  decltype(auto) to_tuple() const { return std::tuple{}; }

  Action decide_action() const;
};

inline Agent::Agent(World &the_world, ID id) : Component(the_world, id) {}

} // namespace game::model::components

#endif // GRIMOIRE_MODEL_COMPONENTS_AGENT_H_
