
#include "model/components/location.h"

#include "model/components/map.h"
#include "model/schema.h"

namespace game::model::components {

bool Location::move(const Int3 &movement) {
  Int3 next_position = get_position() + movement;
  auto map = this->world.get_map();
  if (map->is_tile_inside(next_position) && map->is_tile_passable(next_position)) {
    set_position(_position + movement);
    return true;
  }
  return false;
}

size_t Location::squared_distance_to(Handle<const Location> other_location) const {
  auto diff = other_location->get_position() - _position;
  return static_cast<size_t>(diff.dot(diff));
}

bool Location::is_within(Handle<const Location> other_location, size_t distance) const {
  return squared_distance_to(other_location) <= distance * distance;
}

} // namespace game::model::components
