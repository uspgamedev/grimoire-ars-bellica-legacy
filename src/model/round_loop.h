
#ifndef GRIMOIRE_MODEL_ROUND_LOOP_H_
#define GRIMOIRE_MODEL_ROUND_LOOP_H_

#include <deque>

#include "model/identity.h"

namespace game::model {

class World;

class RoundLoop final {
 public:
  enum class Status { USER_ACTION_REQUESTED, NO_PLAYER, REPORT };
  void register_actor(ID id);
  void unregister_actor(ID id);
  /// Returns true if an action was request from the user
  Status run(World &world);

  uint64_t get_time() const;
  void set_time(uint64_t new_time);

 private:
  std::deque<ID> _turn_order{};
  uint64_t _time{};
};

inline void RoundLoop::register_actor(ID id) { _turn_order.push_back(id); }

inline uint64_t RoundLoop::get_time() const { return _time; }

inline void RoundLoop::set_time(uint64_t new_time) { _time = new_time; }

} // namespace game::model

#endif // GRIMOIRE_MODEL_ROUND_LOOP_H_
