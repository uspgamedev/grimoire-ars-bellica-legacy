
#ifndef GRIMOIRE_MODEL_PROFILE_H_
#define GRIMOIRE_MODEL_PROFILE_H_

#include <utility>

namespace game::model {

class Profile final {
 public:
  explicit Profile(std::string name);
  std::string get_name() const;

 private:
  std::string _name;
};

inline Profile::Profile(std::string name) : _name(std::move(name)) {}

inline std::string Profile::get_name() const { return _name; }

} // namespace game::model

#endif // GRIMOIRE_MODEL_PROFILE_H_
