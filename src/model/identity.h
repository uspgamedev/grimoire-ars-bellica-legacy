
#ifndef GRIMOIRE_MODEL_IDENTITY_H
#define GRIMOIRE_MODEL_IDENTITY_H

#include <vector>

#include <cstdint>

namespace game::model {

using ID = uint32_t;

using IDList = std::vector<ID>;

class IDGenerator {
 public:
  IDGenerator() = default;
  IDGenerator(const IDGenerator &) = delete;
  IDGenerator &operator=(const IDGenerator &) = delete;
  IDGenerator(IDGenerator &&) = delete;
  IDGenerator &operator=(IDGenerator &&) = delete;
  ~IDGenerator() = default;

  void restore_last(ID id);
  ID last() const;
  ID next();

 private:
  ID _next_id{};
};

inline void IDGenerator::restore_last(ID id) { _next_id = id + 1; }

inline ID IDGenerator::last() const { return _next_id - 1; }

inline ID IDGenerator::next() { return _next_id++; }

} // namespace game::model

#endif
