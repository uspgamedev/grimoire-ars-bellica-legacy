
#ifndef GRIMOIRE_MODEL_SCHEMA_H_
#define GRIMOIRE_MODEL_SCHEMA_H_

#include "common/datatable.h"
#include "model/appearance.h"
#include "model/identity.h"
#include "model/world.h"

#include <array>
#include <initializer_list>
#include <string>
#include <tuple>
#include <utility>

namespace game::model {

class BaseSchema {
 public:
  virtual ~BaseSchema() = default;
  BaseSchema(const BaseSchema &) = delete;
  BaseSchema &operator=(const BaseSchema &a) = delete;
  BaseSchema(BaseSchema &&) = delete;
  BaseSchema &operator=(BaseSchema &&) = delete;
  virtual bool has_component(const World &world, ID id) const = 0;
  virtual TableSpec to_spec() const = 0;
  virtual void load(World &world, ID id, const DataTable &datatable) const = 0;
  virtual DataTable save(const World &world, ID id) const = 0;

 protected:
  BaseSchema() = default;
};

template <typename T, typename... Args>
class Schema final : public BaseSchema {
 public:
  template <typename... Str>
  explicit Schema(Str... field_names);

  bool has_component(const World &world, ID id) const override;

  TableSpec to_spec() const override;
  template <size_t... Is>
  TableSpec to_spec(std::index_sequence<Is...> /* unused */) const;

  void load(World &world, ID id, const DataTable &datatable) const override;
  template <size_t... Is>
  void load(World &world, ID id, const DataTable &datatable,
            std::index_sequence<Is...> /* unused */) const;

  DataTable save(const World &world, ID id) const override;

 private:
  template <typename T2, typename... Args2>
  friend struct ToDataTable;
  using TupleType = std::tuple<Args...>;
  const std::array<std::string, sizeof...(Args)> _args;
};

template <typename Arg>
inline Arg data_to_type(const DataTable &datatable, const std::string &name) {
  return std::get<Arg>(datatable.at(name));
}

template <typename Arg>
inline void type_to_data(DataTable &datatable, const std::string &name, Arg value) {
  datatable.insert({ name, value });
}

template <typename Arg>
inline FieldValue to_fieldvalue(Arg value) {
  return value;
}

#define DATA_CAST_FROMTO_TYPE(FIELD_T, ARG_T)                                                    \
  template <>                                                                                    \
  inline ARG_T data_to_type<ARG_T>(const DataTable &datatable, const std::string &name) {        \
    return static_cast<ARG_T>(std::get<FIELD_T>(datatable.at(name)));                            \
  }                                                                                              \
  template <>                                                                                    \
  inline void type_to_data<ARG_T>(DataTable & datatable, const std::string &name, ARG_T value) { \
    datatable.insert({ name, static_cast<FIELD_T>(value) });                                     \
  }                                                                                              \
  template <>                                                                                    \
  inline FieldValue to_fieldvalue<ARG_T>(ARG_T value) {                                          \
    return static_cast<FIELD_T>(value);                                                          \
  }

DATA_CAST_FROMTO_TYPE(int, Appearance)
DATA_CAST_FROMTO_TYPE(int, size_t)

#undef DATA_CAST_FROMTO_TYPE

template <typename T, typename... Args>
inline bool Schema<T, Args...>::has_component(const World &world, ID id) const {
  return world.get_component<T>(id).is_valid();
}

template <typename T, typename... Args>
inline TableSpec Schema<T, Args...>::to_spec() const {
  return to_spec(std::index_sequence_for<Args...>{});
}

template <typename T, typename... Args>
template <size_t... Is>
inline TableSpec Schema<T, Args...>::to_spec(std::index_sequence<Is...> /* unused */) const {
  TableSpec specs;
  (specs.emplace_back(
       FieldSpec{ std::get<Is>(_args), to_fieldvalue<std::tuple_element_t<Is, TupleType>>({}) }),
   ...);
  return specs;
}

template <typename T, typename... Args>
template <typename... Str>
inline Schema<T, Args...>::Schema(Str... field_names) : _args{ { field_names... } } {}

template <typename T, typename... Args>
inline void Schema<T, Args...>::load(World &world, ID id, const DataTable &datatable) const {
  load(world, id, datatable, std::index_sequence_for<Args...>{});
}

template <typename T, typename... Args>
template <size_t... Is>
inline void Schema<T, Args...>::load(World &world, ID id, const DataTable &datatable,
                                     std::index_sequence<Is...> /* unused */) const {
  world.register_component<T>(
      id, data_to_type<std::tuple_element_t<Is, TupleType>>(datatable, std::get<Is>(_args))...);
}

template <typename T, typename... Args>
struct ToDataTable final {
  using TupleType = typename Schema<T, Args...>::TupleType;
  template <size_t... Is>
  static DataTable save(const Schema<T, Args...> &schema, const World &world, ID id,
                        std::index_sequence<Is...> /* unused */) {
    DataTable datatable;
    (type_to_data<std::tuple_element_t<Is, TupleType>>(datatable, std::get<Is>(schema._args),
                                                       std::get<Is>(
                                                           world.get_component<T>(id)->to_tuple())),
     ...);
    return datatable;
  }
};

template <typename T>
struct ToDataTable<T> final {
  template <size_t... Is>
  static DataTable save(const Schema<T> & /* unused */, const World & /* unused */, ID /* unused */,
                        std::index_sequence<Is...> /* unused */) {
    return DataTable{};
  }
};

template <typename T, typename... Args>
inline DataTable Schema<T, Args...>::save(const World &world, ID id) const {
  return ToDataTable<T, Args...>::save(*this, world, id, std::index_sequence_for<Args...>{});
}

} // namespace game::model

#endif // GRIMOIRE_MODEL_SCHEMA_H_
