
#ifndef GRIMOIRE_MODEL_GLYPH_PALETTE_H_
#define GRIMOIRE_MODEL_GLYPH_PALETTE_H_

#include "model/appearance.h"
#include "model/tile_map.h"

#include <unordered_map>

namespace game::model {

class GlyphPalette {
 public:
  GlyphPalette();
  wchar_t tile_to_glyph(TileMap::TileID id) const;
  TileMap::TileID glyph_to_tile(wchar_t glyph) const;
  wchar_t appearance_to_glyph(Appearance appearance) const;
  Appearance glyph_to_appearance(wchar_t glyph) const;

 private:
  std::unordered_map<TileMap::TileID, wchar_t> _tile_to_glyph;
  std::unordered_map<wchar_t, TileMap::TileID> _glyph_to_tile;
  std::unordered_map<Appearance, wchar_t> _appearance_to_glyph;
  std::unordered_map<wchar_t, Appearance> _glyph_to_appearance;
};

inline wchar_t GlyphPalette::tile_to_glyph(TileMap::TileID id) const {
  return _tile_to_glyph.at(id);
}

inline TileMap::TileID GlyphPalette::glyph_to_tile(wchar_t glyph) const {
  return _glyph_to_tile.at(glyph);
}

inline wchar_t GlyphPalette::appearance_to_glyph(Appearance appearance) const {
  return _appearance_to_glyph.at(appearance);
}

inline Appearance GlyphPalette::glyph_to_appearance(wchar_t glyph) const {
  return _glyph_to_appearance.at(glyph);
}

} // namespace game::model

#endif
