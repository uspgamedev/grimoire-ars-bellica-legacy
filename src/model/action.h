
#ifndef GRIMOIRE_MODEL_ACTION_H_
#define GRIMOIRE_MODEL_ACTION_H_

#include "common/directions.h"
#include "model/identity.h"

#include <functional>

namespace game::model {

class World;

using Effect = std::function<bool(World &, ID)>;

struct Action {
  size_t cost{ 0 };
  Effect execute{};
};

namespace actions {

Action MOVE(Direction dir);
Action PICK_UP();
Action DROP_ITEM(ID item_id);
Action TOGGLE_DOOR(ID target_id);

} // namespace actions

} // namespace game::model

#endif // GRIMOIRE_MODEL_ACTION_H_
