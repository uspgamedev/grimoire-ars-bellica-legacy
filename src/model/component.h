
#ifndef GRIMOIRE_MODEL_COMPONENT_H
#define GRIMOIRE_MODEL_COMPONENT_H

#include "model/identity.h"
#include "model/world.h"

namespace game::model {

class Session;

class Component {
 public:
  Component(World &the_world, ID id);
  Component(const Component &) = default;
  Component &operator=(const Component &a) = delete;
  Component(Component &&) = default;
  Component &operator=(Component &&) = delete;
  virtual ~Component() = default;

  ID get_id() const;

  template <typename T>
  Handle<T> select();

  template <typename T>
  Handle<const T> select() const;

  World &world;

 private:
  const ID _id;
};

inline Component::Component(World &the_world, ID id) : world(the_world), _id(id) {}

inline ID Component::get_id() const { return _id; }

template <typename T>
inline Handle<T> Component::select() {
  return this->world.get_component<T>(_id);
}

template <typename T>
inline Handle<const T> Component::select() const {
  return this->world.get_component<const T>(_id);
}

} // namespace game::model

#endif
