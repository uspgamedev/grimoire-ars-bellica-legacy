
#include "model/tile_map.h"

#include "common/intn.h"
#include "common/log.h"

#include <sstream>
#include <string>
#include <vector>

namespace game::model {

namespace {

constexpr char NUMBER_REPR(char c) {
  return static_cast<char>(static_cast<int>(c) - static_cast<int>('0'));
}

constexpr TileMap::TileID READ_DEC(char a, char b) {
  return static_cast<TileMap::TileID>(NUMBER_REPR(a) * 10 + NUMBER_REPR(b));
}

const std::string write_dec(TileMap::TileID t) {
  return std::string{ static_cast<char>(static_cast<int>(t) / 10 + static_cast<int>('0')),
                      static_cast<char>(static_cast<int>(t) % 10 + static_cast<int>('0')) };
}

} // namespace

// Constructor
TileMap::TileMap(const std::string &tile_data, size_t w, size_t h, size_t d) : _w(w), _h(h), _d(d) {
  Log parse_log("TileMap Parser");
  parse_log("Parsing map tile data!");
  try {
    std::stringstream stream{ tile_data, std::ios_base::in };
    const size_t len = _w * _h * _d;
    for (size_t i = 0; i < len; i++) {
      if (stream.eof()) throw std::range_error("TileMap data too small for given dimensions.");
      char first{}, second{};
      stream >> first >> second;
      _tiles.push_back(READ_DEC(first, second));
    }
  } catch (std::exception &e) {
    parse_log("ERROR: %", e.what());
  }
}

// Dimensions
size_t TileMap::get_width() const { return _w; }
size_t TileMap::get_height() const { return _h; }
size_t TileMap::get_depth() const { return _d; }

// Get tile at position
TileMap::TileID TileMap::get_tile(const Int3 &pos) const {
  return _tiles.at(get_index(pos.get_i(), pos.get_j(), pos.get_k()));
}

// Set tile at position
void TileMap::set_tile(const Int3 &pos, TileMap::TileID tile_id) {
  _tiles[get_index(pos.get_i(), pos.get_j(), pos.get_k())] = tile_id;
}

// Check if tile is inside map
bool TileMap::is_tile_inside(const Int3 &pos) const {
  return pos.get_i() < _h && pos.get_j() < _w && pos.get_k() < _d;
}

// Generate tile data from tiles (for saving)
std::string TileMap::get_tile_data() const {
  std::string data{};
  for (size_t k = 0; k < _d; k++) {
    for (size_t i = 0; i < _h; i++) {
      for (size_t j = 0; j < _w; j++) {
        data.append(write_dec(get_tile(to_int3(j, i, k))));
      }
      data.append("\n");
    }
    data.append("\n\n");
  }
  return data;
}

// Get real index from coordinates
size_t TileMap::get_index(size_t i, size_t j, size_t k) const { return k * (_w * _h) + i * _w + j; }

} // namespace game::model
