
#include "session.h"

#include "common/appdata.h"
#include "common/datatable.h"
#include "common/directions.h"
#include "common/log.h"
#include "model/load_session.h"
#include "model/schema.h"
#include "model/schema_list.h"

namespace game::model {

namespace {
namespace fs = std::filesystem;
using components::Actor;
using std::istringstream;
using std::ofstream;
using std::string;
using path = fs::path;

} // unnamed namespace

Session::Session(const std::string &name) : _profile(name), _message_log(), _log("session") {
  world.init_element_pool<components::Actor>();
  world.init_element_pool<components::Attacker>();
  world.init_element_pool<components::Agent>();
  world.init_element_pool<components::Carryable>();
  world.init_element_pool<components::Container>();
  world.init_element_pool<components::Location>();
  world.init_element_pool<components::Damageable>();
  world.init_element_pool<components::Manipulator>();
  world.init_element_pool<components::Map>();
  world.init_element_pool<components::Named>();
  world.init_element_pool<components::Openable>();
  world.init_element_pool<components::Player>();
  world.init_element_pool<components::Seer>();
  world.init_element_pool<components::Visible>();
}

RoundLoop::Status Session::round_loop() { return _round_loop.run(this->world); }

ID Session::create_entity(const path &datafile_path) {
  ID id = world.next_id();
  load_components_for_entity(this->world, id, datafile_path);
  return id;
}

} // namespace game::model
