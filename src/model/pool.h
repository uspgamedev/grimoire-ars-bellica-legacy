
#ifndef GRIMOIRE_COMMON_POOL_H_
#define GRIMOIRE_COMMON_POOL_H_

#include "model/identity.h"

#include <exception>
#include <functional>
#include <optional>
#include <queue>
#include <type_traits>
#include <unordered_map>
#include <vector>

namespace game::model {

class BasePool {
 public:
  BasePool() = default;
  virtual ~BasePool() = default;
  BasePool(const BasePool &) = delete;
  BasePool &operator=(const BasePool &a) = delete;
  BasePool(BasePool &&) = delete;
  BasePool &operator=(BasePool &&) = delete;
};

template <typename Object>
class Pool;

// This type is used to determine the constness of a Handle's corresponding Pool type
// The relation is as follows:
// + const Pool provides Handle<const Object>
// + Pool provides Handle<Object>
template <typename Object, bool>
struct PoolTypeDeduce;

template <typename Object>
struct PoolTypeDeduce<Object, true> {
  using type = const Pool<Object>;
};

template <typename Object>
struct PoolTypeDeduce<Object, false> {
  using type = Pool<Object>;
};

class nullhandle_t;

template <typename Object>
class Handle final {
 public:
  using ObjectType = typename std::remove_const<Object>::type;
  using PoolType = typename PoolTypeDeduce<ObjectType, std::is_const<Object>::value>::type;
  Handle() = default;
  Handle(const Handle<Object> &rhs) = default;
  Handle<Object> &operator=(const Handle<Object> &rhs) = default;
  Handle(Handle &&) = delete;
  Handle &operator=(Handle &&) = delete;
  ~Handle() = default;
  bool operator==(const nullhandle_t &rhs) const;
  bool operator!=(const nullhandle_t &rhs) const;

  bool operator==(const Handle<Object> &rhs) const;
  bool operator!=(const Handle<Object> &rhs) const;
  // NOLINTNEXTLINE(hicpp-explicit-conversions)
  operator bool() const;

  Object *operator->() const;
  bool is_valid() const;
  Handle<const ObjectType> to_const() const;

 private:
  friend class Pool<ObjectType>;
  friend class Handle<ObjectType>;
  Handle(PoolType *pool, size_t index, ID id);
  Object *get() const;
  PoolType *const _pool{ nullptr };
  const size_t _index{ 0 };
  const ID _id{ 0 };
};

template <typename Object>
const Handle<Object> empty_handle = Handle<Object>();

class nullhandle_t {
 public:
  nullhandle_t() = default;

  template <typename Object>
  // NOLINTNEXTLINE(hicpp-explicit-conversions)
  operator Handle<Object>() const;

  template <typename Object>
  bool operator==(const Handle<Object> &rhs) const;

  template <typename Object>
  bool operator!=(const Handle<Object> &rhs) const;
};

const nullhandle_t nullhandle;

template <typename Object>
class Pool final : public BasePool {
 public:
  using RefMap = std::unordered_map<ID, size_t>;
  class iterator {
   public:
    iterator(Pool<Object> *pool, const RefMap::iterator &it);
    iterator &operator++();
    Handle<Object> operator*();
    bool operator!=(const iterator &rhs) const;

   private:
    Pool<Object> *_pool;
    RefMap::iterator _it;
  };
  class const_iterator {
   public:
    const_iterator(const Pool<Object> *pool, const RefMap::const_iterator &it);
    const_iterator &operator++();
    Handle<const Object> operator*();
    bool operator!=(const const_iterator &rhs) const;

   private:
    const Pool<Object> *_pool;
    RefMap::const_iterator _it;
  };
  Pool() = default;
  Pool(const Pool &) = delete;
  Pool &operator=(const Pool &) = delete;
  Pool(Pool &&) = delete;
  Pool &operator=(Pool &&) = delete;
  ~Pool() override = default;
  Handle<const Object> at(ID id) const;
  Handle<Object> at(ID id);
  template <typename... Args>
  Handle<Object> emplace_new(Args &&... args);
  bool erase(const Handle<Object> &handle);
  size_t size() const;
  iterator begin();
  iterator end();
  const_iterator begin() const;
  const_iterator end() const;

 private:
  friend class Handle<Object>;
  friend class Handle<const Object>;
  friend class iterator;
  std::vector<std::optional<Object>> _table{};
  std::priority_queue<size_t, std::vector<size_t>, std::greater<>> _freelist{};
  RefMap _refmap{};
};

// Handle implementation

template <typename Object>
inline Handle<Object>::Handle(Handle<Object>::PoolType *pool, size_t index, ID id)
    : _pool(pool), _index(index), _id(id) {}

template <typename Object>
inline Object *Handle<Object>::operator->() const {
  auto object = get();
  if (object == nullptr) throw std::out_of_range("Attempt to access nullhandle");
  return object;
}

template <typename Object>
inline Handle<Object>::operator bool() const {
  return (*this) != nullhandle;
}

template <typename Object>
inline bool Handle<Object>::operator==(const nullhandle_t & /*rhs*/) const {
  return nullhandle == *this;
}

template <typename Object>
inline bool Handle<Object>::operator!=(const nullhandle_t & /*rhs*/) const {
  // it is unsued because all nullhandles are the same
  return nullhandle != *this;
}

template <typename Object>
inline bool Handle<Object>::operator==(const Handle<Object> &rhs) const {
  return _pool == rhs._pool && _id == rhs._id && _index == rhs._index;
}

template <typename Object>
inline bool Handle<Object>::operator!=(const Handle<Object> &rhs) const {
  return !((*this) == rhs);
}

template <typename Object>
inline bool Handle<Object>::is_valid() const {
  return get() != nullptr;
}

template <typename Object>
inline Handle<const typename Handle<Object>::ObjectType> Handle<Object>::to_const() const {
  return Handle<const ObjectType>(_pool, _index, _id);
}

template <typename Object>
inline Object *Handle<Object>::get() const {
  if (_pool == nullptr) return nullptr;
  auto &maybe = _pool->_table.at(_index);
  if (maybe.has_value() && maybe->get_id() == _id) return &maybe.value();
  return nullptr;
}

// nullhande implementation

template <typename Object>
inline nullhandle_t::operator Handle<Object>() const {
  return empty_handle<Object>;
}

template <typename Object>
inline bool nullhandle_t::operator==(const Handle<Object> &rhs) const {
  return rhs == empty_handle<Object>;
}

template <typename Object>
inline bool nullhandle_t::operator!=(const Handle<Object> &rhs) const {
  return rhs != empty_handle<Object>;
}

// Pool iterator implementation

template <typename Object>
inline Pool<Object>::iterator::iterator(Pool<Object> *pool, const RefMap::iterator &it)
    : _pool(pool), _it(it) {}

template <typename Object>
inline typename Pool<Object>::iterator &Pool<Object>::iterator::operator++() {
  ++_it;
  return *this;
}

template <typename Object>
inline Handle<Object> Pool<Object>::iterator::operator*() {
  auto &maybe = _pool->_table.at(_it->second);
  if (maybe.has_value()) return Handle<Object>(_pool, _it->second, maybe->get_id());
  throw std::out_of_range("Invalid pool iterator");
  return nullhandle;
}

template <typename Object>
inline bool Pool<Object>::iterator::operator!=(const iterator &rhs) const {
  return _pool != rhs._pool || _it != rhs._it;
}

template <typename Object>
inline Pool<Object>::const_iterator::const_iterator(const Pool<Object> *pool,
                                                    const RefMap::const_iterator &it)
    : _pool(pool), _it(it) {}

template <typename Object>
inline typename Pool<Object>::const_iterator &Pool<Object>::const_iterator::operator++() {
  ++_it;
  return *this;
}

template <typename Object>
inline Handle<const Object> Pool<Object>::const_iterator::operator*() {
  auto &maybe = _pool->_table.at(_it->second);
  if (maybe.has_value()) return Handle<const Object>(_pool, _it->second, maybe->get_id());
  throw std::out_of_range("Invalid pool iterator");
  return nullhandle;
}

template <typename Object>
inline bool Pool<Object>::const_iterator::operator!=(const const_iterator &rhs) const {
  return _pool != rhs._pool || _it != rhs._it;
}

// Pool implementation

template <typename Object>
inline Handle<const Object> Pool<Object>::at(ID id) const {
  if (const auto &it = _refmap.find(id); it != _refmap.end())
    return Handle<const Object>(this, it->second, id);
  return nullhandle;
}

template <typename Object>
inline Handle<Object> Pool<Object>::at(ID id) {
  if (const auto &it = _refmap.find(id); it != _refmap.end())
    return Handle<Object>(this, it->second, id);
  return nullhandle;
}

template <typename Object>
template <typename... Args>
inline Handle<Object> Pool<Object>::emplace_new(Args &&... args) {
  size_t new_index{ 0 };
  if (!_freelist.empty()) {
    new_index = _freelist.top();
    _freelist.pop();
    _table.at(new_index).emplace(std::forward<Args>(args)...);
  } else {
    new_index = _table.size();
    _table.emplace_back(std::in_place, std::forward<Args>(args)...);
  }
  ID id = _table.at(new_index)->get_id();
  _refmap.insert({ id, new_index });
  return Handle<Object>(this, new_index, id);
}

template <typename Object>
inline bool Pool<Object>::erase(const Handle<Object> &handle) {
  if (handle != nullhandle) {
    _refmap.erase(handle._id);
    _table.at(handle._index) = std::nullopt;
    _freelist.push(handle._index);
    return true;
  }
  return false;
}

template <typename Object>
inline size_t Pool<Object>::size() const {
  return _table.size() - _freelist.size();
}

template <typename Object>
inline typename Pool<Object>::iterator Pool<Object>::begin() {
  return iterator(this, _refmap.begin());
}

template <typename Object>
inline typename Pool<Object>::iterator Pool<Object>::end() {
  return iterator(this, _refmap.end());
}

template <typename Object>
inline typename Pool<Object>::const_iterator Pool<Object>::begin() const {
  return const_iterator(this, _refmap.begin());
}

template <typename Object>
inline typename Pool<Object>::const_iterator Pool<Object>::end() const {
  return const_iterator(this, _refmap.end());
}

} // namespace game::model

#endif // GRIMOIRE_COMMON_POOL_H_
