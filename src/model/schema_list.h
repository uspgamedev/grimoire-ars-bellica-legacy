
#ifndef GRIMOIRE_MODEL_SCHEMA_LIST_H
#define GRIMOIRE_MODEL_SCHEMA_LIST_H

#include "model/components/actor.h"
#include "model/components/agent.h"
#include "model/components/attacker.h"
#include "model/components/carryable.h"
#include "model/components/container.h"
#include "model/components/damageable.h"
#include "model/components/location.h"
#include "model/components/manipulator.h"
#include "model/components/map.h"
#include "model/components/named.h"
#include "model/components/openable.h"
#include "model/components/player.h"
#include "model/components/seer.h"
#include "model/components/visible.h"
#include "model/schema.h"

#include <string>
#include <unordered_map>

namespace game::model {

using ComponentSchemata = const std::unordered_map<std::string, const BaseSchema *>;

ComponentSchemata &component_schemata();

} // namespace game::model

#endif
