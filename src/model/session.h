
#ifndef GRIMOIRE_SESSION_H_
#define GRIMOIRE_SESSION_H_

#include "common/log.h"
#include "model/identity.h"
#include "model/message_log.h"
#include "model/pool.h"
#include "model/profile.h"
#include "model/round_loop.h"
#include "model/world.h"

#include "portable/filesystem.h"
#include <string>
#include <utility>
#include <vector>

namespace game::model {

namespace components {
class Map;
class Player;
} // namespace components

struct Entity;

class Session {
 public:
  explicit Session(const std::string &name);
  Session(const Session &) = delete;
  Session &operator=(const Session &a) = delete;
  Session(Session &&) = delete;
  Session &operator=(Session &&) = delete;
  ~Session() = default;

  void message(const std::wstring &entry);
  std::wstring message_entry(int idx) const;

  RoundLoop::Status round_loop();

  ID create_entity(const std::filesystem::path &datafile_path);

  World world{};

 private:
  friend void load_session(Session &session);
  friend void save_session(const Session &session);

  Profile _profile;
  MessageLog _message_log;
  Log _log;
  RoundLoop _round_loop{};
};

inline void Session::message(const std::wstring &entry) { _message_log.message(entry); }

inline std::wstring Session::message_entry(int idx) const {
  return _message_log.message_entry(idx);
}

} // namespace game::model

#endif // GRIMOIRE_SESSION_H_
