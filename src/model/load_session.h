
#ifndef GRIMOIRE_MODEL_SESSION_LOADER_H_
#define GRIMOIRE_MODEL_SESSION_LOADER_H_

#include "common/datatable.h"
#include "model/identity.h"

#include <string>

namespace game::model {

class Session;
class World;

void create_session_file(const std::string &name);

void load_session(Session &session);

void save_session(const Session &session);

void load_components_for_entity(World &world, ID id, const std::string &datafile_path);

} // namespace game::model

#endif // GRIMOIRE_MODEL_SESSION_LOADER_H_
