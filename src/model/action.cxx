
#include "model/action.h"

#include "common/intn.h"
#include "common/log.h"
#include "model/components/carryable.h"
#include "model/components/container.h"
#include "model/components/location.h"
#include "model/components/manipulator.h"
#include "model/components/map.h"
#include "model/components/openable.h"
#include "model/components/visible.h"
#include "model/world.h"

namespace game::model::actions {

using components::Carryable;
using components::Container;
using components::Location;
using components::Manipulator;
using components::Openable;
using components::Visible;

// FIXME issue #58

Action MOVE(Direction dir) {
  auto movement = static_cast<Int3>(dir_to_int2(dir));
  auto squared_distance = static_cast<size_t>(movement.dot(movement));
  return { static_cast<size_t>(50. * std::sqrt(squared_distance)),
           [dir](World &world, ID actor_id) -> bool {
             auto location = world.get_component<Location>(actor_id);
             if (!location) return false;
             return location->move(static_cast<Int3>(dir_to_int2(dir)));
           } };
}

Action PICK_UP() {
  return { 100, [](World &world, ID actor_id) -> bool {
            const static Log LOG("pickup");
            auto actor_loc = world.get_component<const Location>(actor_id);
            auto actor_inv = world.get_component<Container>(actor_id); // non constness required
            auto pos = actor_loc->get_position();

            if (actor_inv == nullhandle) // cannot pick up things without an inventory
              return false;

            for (ID id : world.get_map()->get_entity_ids_at(pos)) {
              auto carryable = world.get_component<const Carryable>(id);
              if (carryable != nullhandle && id != actor_id) {
                world.destroy_component<Location>(id);
                actor_inv->add_item(id);
                return true;
              }
            }
            return false;
          } };
}

Action DROP_ITEM(ID item_id) {
  return { 100, [item_id](World &world, ID actor_id) -> bool {
            const static Log LOG("drop item");
            auto actor_inv = world.get_component<Container>(actor_id); // non constness required

            if (actor_inv == nullhandle) // cannot drop things without an inventory
              return false;
            return actor_inv->drop_item(item_id);
          } };
}

Action TOGGLE_DOOR(ID target_id) {
  return { 100, [target_id](World &world, ID actor_id) -> bool {
            // FIXME: check components
            auto actor_loc = world.get_component<const Location>(actor_id);
            auto actor_manipulator = actor_loc->select<const Manipulator>();

            auto target_location = world.get_component<Location>(target_id);
            auto target_openable = target_location->select<Openable>();
            auto target_pos = target_location->get_position();
            auto target_visible = target_location->select<Visible>();

            auto other_ids = world.get_map()->get_entity_ids_at(target_pos);

            // cannot open non-openables
            if (!target_openable) return false;

            // cannot target a non-bodied entity
            if (!target_location) return false;

            // Can't interact with objects further than reach
            if (!target_location->is_within(actor_loc, actor_manipulator->get_reach()))
              return false;

            // This will "jam" a target closed if there is a impassable object inside a close target
            for (auto other_id : other_ids)
              if (other_id != target_id &&
                  (!world.get_component<Location>(other_id)->is_passable()))
                return false;

            target_openable->toggle_open();

            // you can open and close an invisible door
            if (target_visible) target_visible->set_appearance(target_openable->get_appearence());

            target_location->set_passable(target_openable->is_open());

            return true;
          } };
}

} // namespace game::model::actions
