
#ifndef GRIMOIRE_MODEL_APPEARANCE_H_
#define GRIMOIRE_MODEL_APPEARANCE_H_

#include <vector>

namespace game::model {

enum class Appearance : size_t {
  INDESCRIBABLE,
  PLAYER,
  OPEN_DOOR,
  CLOSED_DOOR,
};

} // namespace game::model

#endif // GRIMOIRE_MODEL_APPEARANCE_H_
