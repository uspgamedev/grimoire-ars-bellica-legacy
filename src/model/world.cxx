
#include "model/world.h"

#include "model/components/map.h"
#include "model/components/player.h"

namespace game::model {

Handle<components::Map> World::get_map() {
  auto map_it = get_pool<components::Map>().begin();
  return map_it != get_pool<components::Map>().end() ? *map_it : nullhandle;
}

Handle<const components::Map> World::get_map() const {
  auto map_it = get_pool<components::Map>().begin();
  return map_it != get_pool<components::Map>().end() ? *map_it : nullhandle;
}

Handle<components::Player> World::get_player() {
  auto player_it = get_pool<components::Player>().begin();
  return player_it != get_pool<components::Player>().end() ? *player_it : nullhandle;
}

Handle<const components::Player> World::get_player() const {
  auto player_it = get_pool<components::Player>().begin();
  return player_it != get_pool<components::Player>().end() ? *player_it : nullhandle;
}

} // namespace game::model
