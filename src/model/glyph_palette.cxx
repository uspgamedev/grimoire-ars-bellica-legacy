
#include "model/glyph_palette.h"

#include "common/appdata.h"
#include "common/log.h"
#include "model/appearance.h"
#include "model/tile_map.h"

#include <cpptoml.h>

#include <codecvt>
#include <fstream>
#include <locale>
#include <string>
#include <vector>

namespace game::model {

namespace {
using std::string;
using std::wfstream;
using std::wstring;
} // unnamed namespace

GlyphPalette::GlyphPalette()
    : _tile_to_glyph(), _glyph_to_tile(), _appearance_to_glyph(), _glyph_to_appearance() {
  static const Log LOG("palette");
  // tiles
  wfstream tile_palette_data(appdata::db_dir() / "tiles.palette");
  tile_palette_data.imbue(std::locale("en_US.UTF-8"));
  LOG("Loading tiles");
  for (size_t i = 0; tile_palette_data.good(); i++) {
    auto tile = static_cast<TileMap::TileID>(i);
    wchar_t glyph{};
    tile_palette_data.get(glyph);
    LOG("Loaded glyph %", glyph);
    _tile_to_glyph[tile] = glyph;
    _glyph_to_tile[glyph] = tile;
  }
  // appearances
  wfstream appearance_palette_data(appdata::db_dir() / "appearances.palette");
  appearance_palette_data.imbue(std::locale("en_US.UTF-8"));
  LOG("Loading appearances");
  for (size_t i = 0; appearance_palette_data.good(); i++) {
    auto appearance = static_cast<Appearance>(i);
    wchar_t glyph{};
    appearance_palette_data.get(glyph);
    LOG("Loaded glyph %", glyph);
    _appearance_to_glyph[appearance] = glyph;
    _glyph_to_appearance[glyph] = appearance;
  }
}

} // namespace game::model
