
#ifndef GRIMOIRE_SESSION_MESSAGE_LOG_H_
#define GRIMOIRE_SESSION_MESSAGE_LOG_H_

#include <string>
#include <vector>

namespace game::model {

class MessageLog {
 public:
  MessageLog(); // we do not save the messages in the .toml, so uswe use the default constructor
  MessageLog(const MessageLog &) = delete;
  MessageLog &operator=(const MessageLog &a) = delete;
  MessageLog(MessageLog &&) = delete;
  MessageLog &operator=(MessageLog &&) = delete;
  ~MessageLog() = default;

  void message(const std::wstring &entry);
  std::wstring message_entry(int idx) const;

 private:
  std::vector<std::wstring> _message_entries;
};

inline MessageLog::MessageLog() : _message_entries() {}

inline void MessageLog::message(const std::wstring &entry) { _message_entries.emplace_back(entry); }

inline std::wstring MessageLog::message_entry(int idx) const {
  auto size = static_cast<int>(_message_entries.size());
  if (idx >= size || -idx > size) {
    return L"";
  }
  return _message_entries[static_cast<size_t>((idx + size) % size)];
}

} // namespace game::model

#endif