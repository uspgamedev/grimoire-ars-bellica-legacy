
#include "model/round_loop.h"
#include "model/action.h"
#include "model/components/actor.h"
#include "model/components/agent.h"
#include "model/components/player.h"
#include "model/world.h"

#include "common/log.h"

#include <algorithm>
#include <optional>

namespace game::model {

namespace {
using std::find;

using components::Actor;
using components::Agent;
using components::Player;
} // namespace

void RoundLoop::unregister_actor(ID id) {
  auto it = find(_turn_order.begin(), _turn_order.end(), id);
  if (it != _turn_order.end()) _turn_order.erase(it);
}

RoundLoop::Status RoundLoop::run(World &world) {
  const static game::Log LOG("round");
  static size_t n = 0;
  std::optional<RoundLoop::Status> loop_status;
  while (true) {
    loop_status = std::nullopt;
    bool no_player = (world.get_pool<Player>().size() == 0);
    LOG("new turn % (player=%)", ++n, !no_player);
    ID next = _turn_order.front();
    auto actor = world.get_component<Actor>(next);
    do {
      if (actor) {
        bool action_happened = false;
        if (actor->is_ready()) {
          Action action;
          if (auto player = world.get_component<Player>(next); player) {
            if (player->has_next_action())
              action = player->pop_next_action();
            else {
              // we need to return here so we don't do actor->tick and _time++
              return Status::USER_ACTION_REQUESTED;
            }
          } else {
            action = world.get_component<Agent>(next)->decide_action();
          }
          action.execute(world, next);
          actor->spend_energy(action.cost);
          action_happened = true;
        }
        _turn_order.pop_front();
        _turn_order.push_back(next);
        if (action_happened) {
          loop_status = Status::REPORT;
          break;
        }
      } else {
        _turn_order.pop_front();
      }
      if (no_player) {
        loop_status = Status::NO_PLAYER;
        break;
      }
    } while (false);
    if (actor) {
      actor->tick();
      _time++;
    }
    if (loop_status) return loop_status.value();
  }
}

} // namespace game::model
