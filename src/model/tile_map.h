
#ifndef GRIMOIRE_MODEL_TILEMAP_H_
#define GRIMOIRE_MODEL_TILEMAP_H_

#include "common/intn.h"

#include <string>
#include <vector>

namespace game::model {

class TileMap {
 public:
  // Constructors
  explicit TileMap(const std::string &tile_data, size_t w, size_t h, size_t d);

  // Inner class
  enum class TileID : uint8_t { EMPTY = 0, ROCK = 1 };

  // Public Methods
  std::string get_tile_data() const;
  TileID get_tile(const Int3 &pos) const;
  void set_tile(const Int3 &pos, TileID tile_id);
  bool is_tile_inside(const Int3 &pos) const;
  size_t get_width() const;
  size_t get_height() const;
  size_t get_depth() const;

 private:
  // Private Methods
  size_t get_index(size_t i, size_t j, size_t k) const;

  // Private Attributes
  const size_t _w, _h, _d;
  std::vector<TileID> _tiles{};
};

} // namespace game::model

#endif
