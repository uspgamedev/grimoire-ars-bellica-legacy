
#ifndef GRIMOIRE_WORLD_H_
#define GRIMOIRE_WORLD_H_

#include "model/identity.h"
#include "model/pool.h"

#include <memory>
#include <type_traits>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

namespace game::model {

class Session;

namespace components {
class Map;
class Player;
} // namespace components

class World final {
 public:
  World(const World &) = delete;
  World &operator=(const World &a) = delete;
  World(World &&) = delete;
  World &operator=(World &&) = delete;
  World() = default;
  ~World() = default;

  template <typename T>
  Pool<T> &get_pool();
  template <typename T>
  const Pool<T> &get_pool() const;

  template <typename T, typename... Args>
  Handle<T> register_component(ID id, Args &&... args);

  template <typename T>
  void destroy_component(ID id);

  template <typename T>
  Handle<T> get_component(ID id);

  template <typename T>
  Handle<const T> get_component(ID id) const;

  template <typename T>
  bool exists(ID id) const;

  Handle<components::Map> get_map();
  Handle<const components::Map> get_map() const;

  Handle<components::Player> get_player();
  Handle<const components::Player> get_player() const;

  template <typename T>
  void init_element_pool();

  void restore_last_id(ID id);
  ID last_id() const;
  ID next_id();

 private:
  std::unordered_map<std::type_index, std::unique_ptr<BasePool>> _pools{};
  IDGenerator _id_generator{};
};

template <typename T>
inline Pool<T> &World::get_pool() {
  static_assert(!std::is_const<T>::value, "const-qualified template not allowed for Pool<T>");
  auto type_index = std::type_index(typeid(T));
  return *dynamic_cast<Pool<T> *>(_pools.at(type_index).get());
}
template <typename T>
inline const Pool<T> &World::get_pool() const {
  static_assert(!std::is_const<T>::value, "const-qualified template not allowed for Pool<T>");
  auto type_index = std::type_index(typeid(T));
  return *dynamic_cast<Pool<T> *>(_pools.at(type_index).get());
}

template <typename T, typename... Args>
inline Handle<T> World::register_component(ID id, Args &&... args) {
  return get_pool<T>().emplace_new(*this, id, std::forward<Args>(args)...);
}

template <typename T>
inline void World::destroy_component(ID id) {
  get_pool<T>().erase(get_pool<T>().at(id));
}

template <typename T>
inline Handle<T> World::get_component(ID id) {
  // constexpr essentially turns this into an #ifdef
  if constexpr (std::is_const<T>::value)
    return static_cast<const World *>(this)->get_component<T>(id);
  else // NOLINT because we really don't want to compile the line below in this case
    return get_pool<T>().at(id);
}

template <typename T>
inline Handle<const T> World::get_component(ID id) const {
  // We need to remove the const because we can't have Pool<const T>
  return get_pool<std::remove_const_t<T>>().at(id);
}

template <typename T>
inline bool World::exists(ID id) const {
  return get_component<T>(id) != nullhandle;
}

template <typename T>
inline void World::init_element_pool() {
  _pools.insert({ std::type_index(typeid(T)), std::make_unique<Pool<T>>() });
}

inline void World::restore_last_id(ID id) { _id_generator.restore_last(id); }

inline ID World::last_id() const { return _id_generator.last(); }

inline ID World::next_id() { return _id_generator.next(); }

} // namespace game::model

#endif // GRIMOIRE_WORLD_H_
