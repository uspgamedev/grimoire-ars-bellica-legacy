
#include "model/schema_list.h"

namespace game::model {

ComponentSchemata &component_schemata() {
  static ComponentSchemata schemas{
    { "actor", &components::Actor::schema() },
    { "agent", &components::Agent::schema() },
    { "attacker", &components::Attacker::schema() },
    { "carryable", &components::Carryable::schema() },
    { "container", &components::Container::schema() },
    { "location", &components::Location::schema() },
    { "damageable", &components::Damageable::schema() },
    { "manipulator", &components::Manipulator::schema() },
    { "map", &components::Map::schema() },
    { "named", &components::Named::schema() },
    { "openable", &components::Openable::schema() },
    { "player", &components::Player::schema() },
    { "seer", &components::Seer::schema() },
    { "visible", &components::Visible::schema() },
  };
  return schemas;
}

namespace components {

const BaseSchema &Actor::schema() {
  const static Schema<Actor, size_t, size_t> schema{ "speed", "energy" };
  return schema;
}

const BaseSchema &Agent::schema() {
  const static Schema<Agent> schema{};
  return schema;
}

const BaseSchema &Attacker::schema() {
  const static Schema<Attacker, size_t, size_t> schema{ "power", "skill" };
  return schema;
}

const BaseSchema &Carryable::schema() {
  const static Schema<Carryable, size_t> schema{ "weight" };
  return schema;
}

const BaseSchema &Container::schema() {
  const static Schema<Container, IDList> schema{ "contents" };
  return schema;
}

const BaseSchema &Location::schema() {
  const static Schema<Location, Int3, bool> schema("position", "passable");
  return schema;
}

const BaseSchema &Damageable::schema() {
  const static Schema<Damageable, size_t, size_t> schema{ "max_hp", "damage" };
  return schema;
}

const BaseSchema &Manipulator::schema() {
  const static Schema<Manipulator, size_t> schema{ "reach" };
  return schema;
}

const BaseSchema &Map::schema() {
  const static Schema<Map, std::string, size_t, size_t, size_t> schema{ "tile_data", "width",
                                                                        "height", "depth" };
  return schema;
}

const BaseSchema &Named::schema() {
  const static Schema<Named, std::string> schema("name");
  return schema;
}

const BaseSchema &Openable::schema() {
  const static Schema<Openable, bool, Appearance, Appearance> schema{ "open", "opened_look",
                                                                      "closed_look" };
  return schema;
}

const BaseSchema &Player::schema() {
  const static Schema<Player> schema{};
  return schema;
}

const BaseSchema &Seer::schema() {
  const static Schema<Seer, size_t> schema{ "range" };
  return schema;
}

const BaseSchema &Visible::schema() {
  const static Schema<Visible, Appearance> schema{ "appearance" };
  return schema;
}

} // namespace components

} // namespace game::model
