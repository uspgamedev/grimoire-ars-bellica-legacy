
#include "model/load_session.h"

#include "common/appdata.h"
#include "common/datatable.h"
#include "model/schema_list.h"
#include "model/session.h"

#include <cpptoml.h>

#include "portable/filesystem.h"
#include <fstream>
#include <utility>

namespace game::model {

namespace {

namespace fs = std::filesystem;
using components::Actor;
using std::istringstream;
using std::ofstream;
using std::string;
using path = fs::path;

} // unnamed namespace

void create_session_file(const string &name) {
  const path default_save_path{ appdata::db_dir() / "default_save.toml" };
  const path new_save_path{ appdata::save_dir() / (name + ".toml") };
  fs::copy(default_save_path, new_save_path);
}

void load_session(Session &session) {
  const static game::Log LOG("loading");
  const auto name = session._profile.get_name();
  const path save_path = appdata::save_dir() / (name + ".toml");
  const auto save = cpptoml::parse_file(save_path);

  auto entities_data = save->get_table_array("entities");
  for (const auto &entity_data : *entities_data) {
    auto id = entity_data->get_as<ID>("id").value_or(0);
    LOG("entity %", id);
    for (auto &entry : component_schemata()) {
      auto &schema = entry.second;
      auto comp_data = entity_data->get_table(entry.first);
      LOG("has table %? %", entry.first, comp_data);
      if (comp_data) {
        TableSpec spec = schema->to_spec();
        schema->load(session.world, id, toml_to_data(comp_data, spec));
      }
    }
  }

  // session data
  auto session_data = save->get_table("session");
  session._round_loop.set_time(session_data->get_as<uint64_t>("time").value_or(0));
  session.world.restore_last_id(session_data->get_as<ID>("last_id").value_or(0));

  for (auto actor : session.world.get_pool<Actor>())
    session._round_loop.register_actor(actor->get_id());
}

void save_session(const Session &session) {
  using namespace cpptoml;
  const path save_path{ appdata::save_dir() / (session._profile.get_name() + ".toml") };
  auto root = make_table();
  auto entities = make_table_array();
  auto session_data = make_table();
  root->insert("session", session_data);
  root->insert("entities", entities);
  // session metadata
  session_data->insert("last_id", session.world.last_id());
  session_data->insert("time", session._round_loop.get_time());
  for (const auto named : session.world.get_pool<components::Named>()) {
    auto entity_data = make_table();
    entity_data->insert("id", named->get_id());
    for (auto &entry : component_schemata()) {
      auto &schema = entry.second;
      if (schema->has_component(session.world, named->get_id())) {
        TableSpec spec = schema->to_spec();
        entity_data->insert(entry.first,
                            data_to_toml(schema->save(session.world, named->get_id()), spec));
      }
    }
    entities->push_back(entity_data);
  }
  ofstream(save_path) << (*root);
}

void load_components_for_entity(World &world, ID id, const std::string &datafile_path) {
  path absolute_path = appdata::db_dir() / "entities" / datafile_path;
  absolute_path += ".toml";
  const auto entity_data = cpptoml::parse_file(absolute_path);
  for (auto &entry : component_schemata()) {
    auto &schema = entry.second;
    auto comp_data = entity_data->get_table(entry.first);
    if (comp_data) {
      TableSpec spec = schema->to_spec();
      schema->load(world, id, toml_to_data(comp_data, spec));
    }
  }
}

} // namespace game::model
