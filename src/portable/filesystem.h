
#ifndef GRIMOIRE_PORTABLE_FILESYSTEM_H_
#define GRIMOIRE_PORTABLE_FILESYSTEM_H_

#include "config.h"

#ifdef GRIMOIRE_FILESYSTEM_IS_EXPERIMENTAL

#include <experimental/filesystem>

// NOLINTNEXTLINE(cert-dcl58-cpp)
namespace std {
using namespace experimental;
} // namespace std

#else

#include <filesystem>

#endif

#endif
