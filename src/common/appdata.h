
#ifndef GRIMOIRE_COMMON_APPDATA_H_
#define GRIMOIRE_COMMON_APPDATA_H_

#include "portable/filesystem.h"

#include <string>

namespace game::appdata {

std::filesystem::path root_dir();
std::filesystem::path db_dir();
std::filesystem::path save_dir();
std::filesystem::path settings_path();

bool bootstrap();

} // namespace game::appdata

#endif // GRIMOIRE_COMMON_APPDATA_H_
