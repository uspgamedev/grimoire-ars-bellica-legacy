
#ifndef GRIMOIRE_COMMON_SETTINGS_H_
#define GRIMOIRE_COMMON_SETTINGS_H_

#include "frontend/command.h"

#include <ostream>
#include <string>
#include <unordered_map>

namespace game {

class Settings {
 public:
  Settings();
  std::string get_version() const;
  static void bootstrap(std::ostream &stream);
  const std::unordered_map<char, frontend::Command> &get_keybindings() const;

 private:
  std::string _version;
  std::unordered_map<char, frontend::Command> _keybindings;
};

inline std::string Settings::get_version() const { return _version; }

inline const std::unordered_map<char, frontend::Command> &Settings::get_keybindings() const {
  return _keybindings;
}

} // namespace game

#endif // GRIMOIRE_COMMON_SETTINGS_H_
