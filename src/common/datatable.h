
#ifndef GRIMOIRE_COMMON_DATATABLE_H_
#define GRIMOIRE_COMMON_DATATABLE_H_

#include "common/intn.h"
#include "model/identity.h"

#include <memory>
#include <unordered_map>
#include <variant>
#include <vector>

namespace cpptoml {
class table;
} // namespace cpptoml

namespace game {

using FieldValue = std::variant<bool, int, std::string, Int2, Int3, model::IDList>;

struct FieldSpec {
  const std::string name;
  const FieldValue default_value;
};

using TableSpec = std::vector<FieldSpec>;

using DataTable = std::unordered_map<std::string, FieldValue>;

DataTable
toml_to_data(const std::shared_ptr<cpptoml::table> &tomltable, const TableSpec &tablespec);

std::shared_ptr<cpptoml::table>
data_to_toml(const DataTable &datatable, const TableSpec &tablespec);

} // namespace game

#endif // GRIMOIRE_COMMON_DATATABLE_H_
