
#ifndef GRIMOIRE_COMMON_LOG_H_
#define GRIMOIRE_COMMON_LOG_H_

#include "common/format.h"

#include <string>
#include <utility>

namespace game {

void log(const std::string &tag, const std::string &line);

template <typename... Args>
inline void logf(const std::string &tag, const std::string &fmt, Args... args) {
  log(tag, format(fmt, args...));
}

class Log {
 public:
  explicit Log(const char *tag);
  explicit Log(std::string tag);

  template <typename... Args>
  void operator()(const std::string &fmt, Args... args) const;

 private:
  const std::string _tag;
};

inline Log::Log(const char *tag) : _tag(tag) {}

inline Log::Log(std::string tag) : _tag(std::move(tag)) {}

template <typename... Args>
inline void Log::operator()(const std::string &fmt, Args... args) const {
  game::logf(_tag, fmt, args...);
}

} // namespace game

#endif // GRIMOIRE_COMMON_LOG_H_
