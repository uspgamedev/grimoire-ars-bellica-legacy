
#include "common/log.h"

#include <fstream>
#include <iostream>

namespace game {

namespace {

using std::endl;
using std::ofstream;
using std::string;

constexpr size_t _TAG_WIDTH = 10;

} // unnamed namespace

void log(const string &tag, const string &line) {
  static ofstream out("out", ofstream::app);
  out.width(_TAG_WIDTH);
  out << ("[" + tag + "] ") << line << endl;
}

} // namespace game
