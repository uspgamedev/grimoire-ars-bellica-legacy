
#include "common/settings.h"
#include "common/appdata.h"
#include "common/log.h"
#include "config.h"

#include <cpptoml.h>

#include "portable/filesystem.h"

#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>

namespace game {

namespace {

using game::frontend::Command;

using std::ifstream;
using std::string;
using std::unordered_map;

} // namespace

void Settings::bootstrap(std::ostream &stream) {
  auto data = cpptoml::make_table();

  data->insert("version", GRIMOIRE_VERSION);
  data->insert("keybindings",
               cpptoml::parse_file(appdata::db_dir() / "keybindings.toml")->get_table("default"));
  stream << *data;
}

Settings::Settings() : _version(""), _keybindings{} {
  const static game::Log LOG("settings");
  const auto SETTINGS_PATH = appdata::settings_path();
  auto data = cpptoml::parse_file(SETTINGS_PATH);
  auto keybindings = data->get_table("keybindings");
  const static unordered_map<string, Command> name_to_command{
    { "left", Command::LEFT },
    { "down", Command::DOWN },
    { "up", Command::UP },
    { "right", Command::RIGHT },
    { "upleft", Command::UPLEFT },
    { "upright", Command::UPRIGHT },
    { "downleft", Command::DOWNLEFT },
    { "downright", Command::DOWNRIGHT },
    { "interact", Command::INTERACT },
    { "inspect", Command::INSPECT },
    { "quit", Command::QUIT },
    { "pickup", Command::PICKUP },
  };

  if (keybindings != nullptr)
    for (const auto &pair : *keybindings) {
      string key = pair.first;
      char value = pair.second->as<string>()->get().front();
      LOG("bound % to %", value, static_cast<int>(name_to_command.at(key)));
      _keybindings[value] = name_to_command.at(key);
    }
  else {
    LOG(std::string("Keybindings unspecified, could not proceed"));
    throw(std::runtime_error(
        "In Settings::Settings:\n \"keybindings\" unspecified in settings.toml"));
  }

  _version = data->get_as<string>("version").value_or("unknown");
}

} // namespace game
