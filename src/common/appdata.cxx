
#include "common/appdata.h"

#include "config.h"

#include "common/log.h"
#include "common/settings.h"
#include "portable/filesystem.h"

#include <cstdlib>
#include <fstream>
#include <iostream>

namespace game::appdata {

namespace {

using game::Log;

using std::ifstream;
using std::string;
namespace fs = std::filesystem;

string _home() {
  if (GRIMOIRE_IS_WINDOWS) return std::getenv("APPDATA");
  return std::getenv("HOME");
}

} // unnamed namespace

fs::path root_dir() { return fs::path(_home()) / GRIMOIRE_APPDATA_PATH; }

fs::path db_dir() { return root_dir() / "database"; }

fs::path save_dir() { return root_dir() / "saves"; }

fs::path settings_path() { return root_dir() / "settings.toml"; }

bool bootstrap() {
  const static Log LOG("appdata");
  const auto DB_DIR = db_dir();
  const auto SAVE_DIR = save_dir();
  // Check root dir
  if (!ifstream(root_dir()).good()) fs::create_directory(root_dir());
  // Check database dir
  if (!ifstream(DB_DIR).good()) {
    LOG("Database not found, searching available sources...");
    const string db_source_path = "./database";
    if (ifstream(db_source_path).good()) {
      LOG("Found source at `%`", db_source_path);
      fs::copy(db_source_path, DB_DIR, fs::copy_options::recursive);
    } else {
      LOG("Could not find database source");
      return false;
    }
  }
  // Check settings file
  const auto SETTINGS_PATH = settings_path();
  if (!ifstream(SETTINGS_PATH).good()) {
    LOG("Settings not found. Creating at `%`", SETTINGS_PATH);
    std::ofstream settings_stream(SETTINGS_PATH);
    Settings::bootstrap(settings_stream);
  }
  // Check save folder
  if (!ifstream(SAVE_DIR).good()) fs::create_directory(SAVE_DIR);
  return true;
}

} // namespace game::appdata
