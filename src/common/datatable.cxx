
#include "common/datatable.h"

#include <cpptoml.h>

#include <vector>

namespace game {

namespace {

using std::get;
using std::visit;

using cpptoml::make_array;
using cpptoml::make_table;

struct TOML2Data {
  template <typename T>
  void operator()(T arg) {
    datatable.insert({ fieldspec.name, tomltable->get_as<T>(fieldspec.name).value_or(arg) });
  }
  void operator()(Int2 arg) {
    auto array = tomltable->get_array_of<int64_t>(fieldspec.name)
                     .value_or(std::vector<int64_t>{ arg.get_x(), arg.get_y() });
    Int2 value(static_cast<int>(array.at(0)), static_cast<int>(array.at(1)));
    datatable.insert({ fieldspec.name, value });
  }
  void operator()(Int3 arg) {
    auto array = tomltable->get_array_of<int64_t>(fieldspec.name)
                     .value_or(std::vector<int64_t>{ arg.get_x(), arg.get_y(), arg.get_z() });
    Int3 value(static_cast<int>(array.at(0)), static_cast<int>(array.at(1)),
               static_cast<int>(array.at(2)));
    datatable.insert({ fieldspec.name, value });
  }
  void operator()(model::IDList arg) {
    std::vector<int64_t> default_value = {};
    model::IDList value = {};

    for (auto &id : arg) default_value.push_back(static_cast<int64_t>(id));

    auto array = tomltable->get_array_of<int64_t>(fieldspec.name).value_or(default_value);

    for (auto &entry : array) value.push_back(static_cast<model::ID>(entry));

    datatable.insert({ fieldspec.name, value });
  }

  const FieldSpec &fieldspec;
  std::shared_ptr<cpptoml::table> tomltable;
  DataTable &datatable;
};

struct Data2TOML {
  template <typename T>
  void operator()(T arg) {
    T value = arg;
    if (const auto &it = datatable.find(fieldspec.name); it != datatable.end())
      value = get<T>(it->second);
    tomltable->insert(fieldspec.name, value);
  }
  void operator()(Int2 arg) {
    Int2 value = arg;
    if (const auto &it = datatable.find(fieldspec.name); it != datatable.end())
      value = get<Int2>(it->second);
    auto array = make_array();
    array->push_back(value.get_x());
    array->push_back(value.get_y());
    tomltable->insert(fieldspec.name, array);
  }
  void operator()(Int3 arg) {
    Int3 value = arg;
    if (const auto &it = datatable.find(fieldspec.name); it != datatable.end())
      value = get<Int3>(it->second);
    auto array = make_array();
    array->push_back(value.get_x());
    array->push_back(value.get_y());
    array->push_back(value.get_z());
    tomltable->insert(fieldspec.name, array);
  }
  void operator()(model::IDList arg) {
    model::IDList value = std::move(arg);
    if (const auto &it = datatable.find(fieldspec.name); it != datatable.end())
      value = get<model::IDList>(it->second);
    auto array = make_array();
    for (auto &id : value) array->push_back(id);
    tomltable->insert(fieldspec.name, array);
  }
  const FieldSpec &fieldspec;
  std::shared_ptr<cpptoml::table> tomltable;
  const DataTable &datatable;
};

} // unnamed namespace

DataTable
toml_to_data(const std::shared_ptr<cpptoml::table> &tomltable, const TableSpec &tablespec) {
  DataTable datatable;
  for (const auto &fieldspec : tablespec)
    visit(TOML2Data{ fieldspec, tomltable, datatable }, fieldspec.default_value);
  return datatable;
}

std::shared_ptr<cpptoml::table>
data_to_toml(const DataTable &datatable, const TableSpec &tablespec) {
  auto tomltable = make_table();
  for (const auto &fieldspec : tablespec)
    visit(Data2TOML{ fieldspec, tomltable, datatable }, fieldspec.default_value);
  return tomltable;
}

} // namespace game
