
#ifndef GRIMOIRE_COMMON_INTVEC_H_
#define GRIMOIRE_COMMON_INTVEC_H_

#include <array>
#include <numeric>
#include <ostream>
#include <string>

#include <cmath>

namespace game {

template <size_t dim>
class IntVec final {
 public:
  template <typename... Args>
  explicit IntVec(Args... args) noexcept;
  template <size_t N>
  explicit IntVec(const IntVec<N> &rhs);
  IntVec();

  int &operator[](size_t coord);
  const int &operator[](size_t coord) const;

  IntVec<dim> operator+(const IntVec<dim> &rhs) const;
  IntVec<dim> operator-(const IntVec<dim> &rhs) const;
  IntVec<dim> operator*(const IntVec<dim> &rhs) const;

  IntVec<dim> operator-() const;

  IntVec<dim> operator*(int scalar) const;
  IntVec<dim> operator/(int scalar) const;

  IntVec<dim> &operator+=(const IntVec<dim> &rhs);
  IntVec<dim> &operator-=(const IntVec<dim> &rhs);

  bool operator==(const IntVec<dim> &rhs) const;

  size_t size() const;
  IntVec<dim> unit() const;
  int dot(const IntVec<dim> &rhs) const;

  void set_x(int x);
  void set_y(int y);
  void set_z(int z);
  void set_i(size_t i);
  void set_j(size_t j);
  void set_k(size_t k);

  int get_x() const;
  int get_y() const;
  int get_z() const;
  size_t get_i() const;
  size_t get_j() const;
  size_t get_k() const;

  IntVec<dim> get_x_component() const;
  IntVec<dim> get_y_component() const;
  IntVec<dim> get_z_component() const;
  IntVec<dim> get_i_component() const;
  IntVec<dim> get_j_component() const;
  IntVec<dim> get_k_component() const;

  IntVec<dim> get_component(size_t n) const;

  static IntVec<dim> unit_x();
  static IntVec<dim> unit_y();
  static IntVec<dim> unit_z();
  static IntVec<dim> unit_i();
  static IntVec<dim> unit_j();
  static IntVec<dim> unit_k();

  static IntVec<dim> unit_n(size_t n);

  template <size_t N>
  int get_n() const;

  int get_n(size_t n) const;

 private:
  std::array<int, dim> _repr;
};

using Int2 = IntVec<2>;
using Int3 = IntVec<3>;

template <size_t dim>
inline IntVec<dim>::IntVec() : _repr() {}

template <size_t dim>
inline int &IntVec<dim>::operator[](size_t coord) {
  return _repr.at(coord);
}

template <size_t dim>
inline const int &IntVec<dim>::operator[](size_t coord) const {
  return _repr.at(coord);
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::operator+(const IntVec<dim> &rhs) const {
  IntVec new_vector;
  for (size_t i = 0; i < dim; i++) new_vector[i] = (*this)[i] + rhs[i];
  return new_vector;
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::operator-(const IntVec<dim> &rhs) const {
  IntVec new_vector;
  for (size_t i = 0; i < dim; i++) new_vector[i] = (*this)[i] - rhs[i];
  return new_vector;
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::operator*(const IntVec<dim> &rhs) const {
  IntVec new_vector;
  for (size_t i = 0; i < dim; i++) new_vector[i] = (*this)[i] * rhs[i];
  return new_vector;
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::operator-() const {
  IntVec new_vector;
  for (size_t i = 0; i < dim; i++) new_vector[i] = -(*this)[i];
  return new_vector;
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::operator*(int scalar) const {
  IntVec new_vector;
  for (size_t i = 0; i < dim; i++) new_vector[i] = (*this)[i] * scalar;
  return new_vector;
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::operator/(int scalar) const {
  IntVec new_vector;
  for (size_t i = 0; i < dim; i++) new_vector[i] = (*this)[i] / scalar;
  return new_vector;
}

template <size_t dim>
inline IntVec<dim> &IntVec<dim>::operator+=(const IntVec<dim> &rhs) {
  for (size_t i = 0; i < dim; i++) (*this)[i] += rhs[i];
  return *this;
}

template <size_t dim>
inline IntVec<dim> &IntVec<dim>::operator-=(const IntVec<dim> &rhs) {
  for (size_t i = 0; i < dim; i++) (*this)[i] -= rhs[i];
  return *this;
}

template <size_t dim>
inline bool IntVec<dim>::operator==(const IntVec<dim> &rhs) const {
  for (size_t i = 0; i < dim; i++)
    if (_repr.at(i) != rhs[i]) return false;
  return true;
}

template <size_t dim>
inline size_t IntVec<dim>::size() const {
  size_t sum = 0;
  for (size_t i = 0; i < dim; i++) sum += static_cast<size_t>(::abs(_repr.at(i)));
  return sum;
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit() const {
  IntVec<dim> other = *this;
  for (size_t i = 0; i < dim; i++) other[i] = std::max(std::min(other[i], 1), -1);
  return other;
}

template <size_t dim>
inline int IntVec<dim>::dot(const IntVec<dim> &rhs) const {
  auto product = *this * rhs;
  return std::accumulate(product._repr.begin(), product._repr.end(), 0);
}

template <size_t dim>
inline void IntVec<dim>::set_x(int x) {
  _repr[0] = x;
}

template <size_t dim>
inline void IntVec<dim>::set_y(int y) {
  _repr[1] = y;
}

template <size_t dim>
inline void IntVec<dim>::set_z(int z) {
  _repr[2] = z;
}

template <size_t dim>
inline void IntVec<dim>::set_i(size_t i) {
  _repr[1] = static_cast<int>(i);
}

template <size_t dim>
inline void IntVec<dim>::set_j(size_t j) {
  _repr[0] = static_cast<int>(j);
}

template <size_t dim>
inline void IntVec<dim>::set_k(size_t k) {
  _repr[2] = static_cast<int>(k);
}

template <size_t dim>
inline int IntVec<dim>::get_x() const {
  return get_n<0>();
}

template <size_t dim>
inline int IntVec<dim>::get_y() const {
  return get_n<1>();
}

template <size_t dim>
inline int IntVec<dim>::get_z() const {
  return get_n<2>();
}

template <size_t dim>
inline size_t IntVec<dim>::get_i() const {
  return static_cast<size_t>(get_n<1>());
}

template <size_t dim>
inline size_t IntVec<dim>::get_j() const {
  return static_cast<size_t>(get_n<0>());
}

template <size_t dim>
inline size_t IntVec<dim>::get_k() const {
  return static_cast<size_t>(get_n<2>());
}

template <size_t dim>
template <size_t N>
inline int IntVec<dim>::get_n() const {
  if (N < dim) return _repr[N];
  return 0;
}

template <size_t dim>
inline int IntVec<dim>::get_n(size_t n) const {
  for (size_t i = 0; i < dim; i++)
    if (i == n) return _repr[i];
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_component(size_t n) const {
  return IntVec<dim>::unit(n) * get_n();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_x_component() const {
  return IntVec<dim>::unit_x() * get_x();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_y_component() const {
  return IntVec<dim>::unit_y() * get_y();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_z_component() const {
  return IntVec<dim>::unit_z() * get_z();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_i_component() const {
  return get_y_component();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_j_component() const {
  return get_x_component();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::get_k_component() const {
  return get_z_component();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_x() {
  return IntVec<dim>(1);
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_y() {
  return IntVec<dim>(0, 1);
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_z() {
  return IntVec<dim>(0, 0, 1);
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_i() {
  return IntVec<dim>::unit_y();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_j() {
  return IntVec<dim>::unit_x();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_k() {
  return IntVec<dim>::unit_z();
}

template <size_t dim>
inline IntVec<dim> IntVec<dim>::unit_n(size_t n) {
  IntVec<dim> result;
  for (size_t i = 0; i < dim; i++)
    if (i == n) result[i] = 1;
  return result;
}

template <size_t dim>
inline std::string to_string(const IntVec<dim> &intn) {
  std::string str = "(";
  for (size_t i = 0; i < dim; i++) {
    str.append(::std::to_string(intn[i]));
    if (i < dim - 1) str.append(", ");
  }
  return str.append(")");
}

template <size_t dim>
inline std::ostream &operator<<(std::ostream &os, const IntVec<dim> &intn) {
  return os << to_string(intn);
}

template <size_t dim>
inline size_t distance(const IntVec<dim> &lhs, const IntVec<dim> &rhs) {
  return (lhs - rhs).size();
}

template <typename T1, typename T2>
inline Int2 to_int2(T1 &&x, T2 &&y) {
  return Int2(static_cast<int>(x), static_cast<int>(y));
}

template <typename T1, typename T2, typename T3>
inline Int3 to_int3(T1 &&x, T2 &&y, T3 &&z) {
  return Int3(static_cast<int>(x), static_cast<int>(y), static_cast<int>(z));
}

template <size_t dim>
inline IntVec<dim> operator*(int scalar, const IntVec<dim> &rhs) {
  return rhs * scalar;
}

template <size_t dim>
template <size_t N>
inline IntVec<dim>::IntVec(const IntVec<N> &rhs) : IntVec() {
  size_t i = 0;
  if (dim < N) {
    for (; i < dim; i++) _repr.at(i) = rhs[i];
  } else {
    for (; i < N; i++) _repr.at(i) = rhs[i];
    for (; i < dim; i++) _repr.at(i) = 0;
  }
}

template <size_t dim>
template <typename... Args>
inline IntVec<dim>::IntVec(Args... args) noexcept : _repr{ { args... } } {}

} // namespace game

#endif // GRIMOIRE_COMMON_INTVEC_H_
