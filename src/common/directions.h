
#ifndef GRIMOIRE_COMMON_DIRECTIONS_H_
#define GRIMOIRE_COMMON_DIRECTIONS_H_

#include "common/intn.h"

#include <array>

namespace game {

enum class Direction : size_t {
  NONE, UP, DOWN, LEFT, RIGHT, UPLEFT, UPRIGHT, DOWNLEFT, DOWNRIGHT
};

const auto DIRECTIONS = std::array<Int2, 9>{ { Int2(0, 0), Int2(0, -1), Int2(0, 1), Int2(-1, 0), Int2(1, 0),
                                    Int2(-1, -1), Int2(-1, 1), Int2(1, -1), Int2(1, 1) } };

inline Int2 dir_to_int2(Direction dir) {
  return DIRECTIONS.at(static_cast<size_t>(dir));
}

inline Direction int2_to_dir(Int2 orientation) {
  for (size_t i = 0; i < DIRECTIONS.size(); i++) {
    if (orientation == DIRECTIONS.at(i))
      return static_cast<Direction>(i);
  }
  return Direction::NONE;
}

} // namespace game

#endif

