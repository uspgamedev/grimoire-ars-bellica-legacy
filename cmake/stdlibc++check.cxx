
/* This file is used to detect whether the compiler uses 'libc++' or 'stdlibc++'.
 * This is important in order to know how to link them appropriately.
 *
 * Particularly, this is relevant due to the filesystem header still being
 * experimental in 'libc++' (which llvm uses). That, and the header in which
 * they store filesystem is different. In 'libc++', the filesystem's compiled
 * code is in 'libc++experimental.a'. In 'stdlibc++' (which g++ uses), the
 * filesystem's compiled code is in 'libc++fs.a'. Thus, the linking flag is
 * different for each implementation of C++'s standard headers.
 *
 * This checkfile works because 'stdlibc++' defines __GLIBCXX__, while 'libc++'
 * does not. This allows us to make code that only compiles in one, but not
 * the other. CMake then sets a flag it was successful in compiling it, and then
 * we know if we're using 'stdlibc++' or 'libc++'.
 * */

#include <iostream>

#ifdef __GLIBCXX__
#define GRIMOIRE_VALID_STDLIBCXX_SYMBOL 1
#endif

int a = GRIMOIRE_VALID_STDLIBCXX_SYMBOL;

int main(int argc, char* argv[]) {
  std::cout << a;
  return 0;
}

