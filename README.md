
# Grimoire: Ars Bellica

A roguelike warfare game with complex magic-systems and automated constructs.


## Status

We are currently following [Rogue Basin's Guide][RBG] on how to make a roguelike.
Also, we are using, for starters, `ncurses` as the game's frontend. We plan on adding
a tile option in the future as well, much like [Dungeon Crawl Stone Soup][DCSS].

[RBG]:https://www.roguebasin.com/index.php?title=How_to_Write_a_Roguelike_in_15_Steps
[DCSS]:https://crawl.develz.org/


## Building From Source

This project is done C++17 and it uses C++17 features. As such it is only supported by the following compilers:

+ LLVM's clang >= 6
+ g++ >= 8

For Mac OSX users, we recommend downloading the [latest llvm][LLVM] and installing it on
`/usr/local`, so as not to conflict with OSX's native AppleClang. **AppleClang as of v9.x,
is not compatible with our code as it does not have full C++17 support.**

Windows building support coming...eventually.

You will also need the following libraries:

+ ncurses (or ncursesw in Ubuntu-based linux distros)
+ cmake >= v3.10

Then build with cmake and run make:

```shell
mkdir build
cd build
cmake .. # add flags and set compiler here
make
```

In Ubuntu-based distros, you need to pass a valid value to the cmake variable
`GRIMOIRE_HAS_NCURSESW`. So the cmake call would be:

```shell
cmake .. -DGRIMOIRE_HAS_NCURSESW=1
```

If `clang >= 6`/`g++ >= 8` is not your default `CXX` compiler,
you must set the `CXX` variable when calling cmake.

```shell
cmake .. -DGRIMOIRE_HAS_NCURSESW=1 CXX=g++-8 # assuming you're using g++-8, running on ubuntu-based linux distro
```

[LLVM]:https://releases.llvm.org/download.html

